# create-initial-turbulence

## Generate turbulent motions

This code package provides software to generate turbulent motions with a given power spectrum. The motions are then stored in binary files and can be used for instance in hydrodynamical simulations as initial turbulence, such as

- [Girichidis et al. 2011](https://ui.adsabs.harvard.edu/abs/2011MNRAS.413.2741G/abstract)
- [Girichidis et al. 2012a](https://ui.adsabs.harvard.edu/abs/2012MNRAS.420..613G/abstract)
- [Girichidis et al. 2012b](https://ui.adsabs.harvard.edu/abs/2012MNRAS.420.3264G/abstract)
- [Girichidis et al. 2021](https://ui.adsabs.harvard.edu/abs/2021MNRAS.505.1083G/abstract)

The code is able to create turbulence with a given ratio of compressive to solenoidal modes, i.e. the relative ratio of compressive to rotational components of the flow. The output can be done in raw binary or hdf5 format.

## components of the code

The package mainly consists of two programs: create-turbulence-field-V2.0.cc and create-turbulence-field-V3.0.cc.

### create-turbulence-field-V2.0.cc

This version is used to generate a random velocity field with a random mix of compressive to solenoidal modes. A detailed projection is not possible. Run the code with `create-turbulence-field-V2.0.cc help` to get an overview of the available parameters.

### create-turbulence-field-V3.0.cc

Creates turbulent motions including a projection in Fourier space to a given ratio of compressive to solenoidal modes. Run the code with `create-turbulence-field-V3.0.cc help` to get an overview of the available parameters.

### read-data

Examples to read the data using python and Fortran.

### test

A simple test to read float and double numbers.
