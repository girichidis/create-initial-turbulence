#!/bin/bash

FOLDER=create-package
TARF=create-turbulence-field

mkdir $FOLDER
mkdir $FOLDER/$TARF
cp create-turbulence-field-V2.0.cc $FOLDER/$TARF/
cp create-turbulence-field-V3.0.cc $FOLDER/$TARF/
cp -L derivs.hh $FOLDER/$TARF/
cp -L histogram.h $FOLDER/$TARF/
cp -L HDFIO.h $FOLDER/$TARF/
cp -L vector-ndim.hh $FOLDER/$TARF/
cp -L create-turbulence.hh $FOLDER/$TARF/
cp Makefile-create $FOLDER/$TARF/Makefile
cp README $FOLDER/$TARF/

cd $FOLDER && tar czf $TARF.tar.gz $TARF/ && mv $TARF.tar.gz ../
