// analyses the power spectrum of a turbulence velocity field
// Philipp Girichidis, Dec 2010
// last change: 13 Dec 2010

#include <iostream>
#include <fstream>
#include <sstream>
#include "vector-ndim.hh"
#include <cmath>
#include <complex>
#include <fftw3.h>


//###############################
//
// MAIN PROGRAMME
//
//###############################

int main(int argc, char* argv[])
{

  // check command line parameter
  if(argc<4)
    {
      std::cout << " usage:  " << argv[0] << "  turb-file   Ndim   flt/dbl" << std::endl;
      std::cout << std::endl;
      std::cout << " Ndim    : number of dimension (=number of data sets)" << std::endl;
      std::cout << " flt/bld : single or double precision data" << std::endl;
      exit(1);
    }


  // output & print parameters
  std::string dim_str[3] = {"x","y","z"};
  

  // analyse command line parameters
  long Ndim             = atoi(argv[2]);
  std::string precision = argv[3];

  
  // open turb file and get total file size
  std::fstream fturb(argv[1], std::ios::in | std::ios::binary);
  fturb.seekg (0, std::ios::end);
  long double ddim;
  if(precision == "flt")
    ddim = pow((long double)fturb.tellg()/(long double)sizeof(float)/(long double)Ndim,1.0/3.0);
  else
    ddim = pow((long double)fturb.tellg()/(long double)sizeof(double)/(long double)Ndim,1.0/3.0);
  if(fabs(ddim - round(ddim)) > 1e-3)
    {
      std::cout << "number of data does not fit: (Ndata/Ndim)^(1/3) = " << ddim << std::endl;
      exit(1);
    }
  long dim = (long)round(ddim);
  size_t Nelements = dim*dim*dim;
  fturb.seekg (0, std::ios::beg);
  std::cout << "  DATA FILE CONSISTS OF " << Ndim << " x " << dim << " " << precision << " values per 3D dimension!" << std::endl;

  // prepare vectors
  arr::v3D<double> k3D(dim,0.0);
  arr::v3D<double> vel(dim);
  arr::v1D<double> kbins_all(dim,0.0);

  // prepare fourier analysis
  fftw_complex *in, *out;
  fftw_plan p;
  in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * Nelements);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * Nelements);
  p   = fftw_plan_dft_3d(dim, dim, dim, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  for(unsigned int dimension = 0; dimension < Ndim; dimension++)
    {
      // info
      std::cout << std::endl;
      std::cout << "  ANALYSE velocity field for dimension " << dimension+1 << std::endl;


      // read in data
      if(precision == "flt")
	{
	  float dummy;
	  long sizeofdummy = sizeof(dummy);
	  for(unsigned long i=0; i<Nelements; i++)
	    {
	      fturb.read(reinterpret_cast<char *>(&dummy), sizeofdummy);
	      in[i][0] = dummy;
	      in[i][1] = 0.0;
	    }
	}
      else
	{
	  double dummy;
	  long sizeofdummy = sizeof(dummy);
	  for(unsigned long i=0; i<Nelements; i++)
	    {
	      fturb.read(reinterpret_cast<char *>(&dummy), sizeofdummy);
	      in[i][0] = dummy;
	      in[i][1] = 0.0;
	    }
	}      
      

      // do fourier transformation
      fftw_execute(p);
      

      // convert output array to more comfortable type
      for(unsigned long i=0; i<Nelements; i++)
	k3D(i) = out[i][0];


      // analyse k-vectors
      arr::v1D<double> kbins(dim,0.0);
      for(long iz=-dim/2+1; iz<dim/2; iz++)
	for(long iy=-dim/2+1; iy<dim/2; iy++)
	  for(long ix=-dim/2+1; ix<dim/2; ix++)
	    {
	      double length = sqrt(pow((double)ix,2.0)+
				   pow((double)iy,2.0)+
				   pow((double)iz,2.0));
	      
	      // power spectrum ~ v(k)^2, therefore k3D^2
	      kbins[(long)round(length)] += pow(k3D(ix,iy,iz),2.0);
	    }
      
      double max = kbins.max();
      kbins.mult(1.0/max);
      kbins_all.add(kbins);

      // print kbins
      std::string dim_extension;
      if(dimension < 3) dim_extension = dim_str[dimension];
      else
	{
	  std::ostringstream s;
	  s << dimension-3;
	  dim_extension = s.str();
	}
      std::fstream kfile(((std::string)argv[1] + "-ana-kspec-" + dim_extension + ".dat").c_str(), std::ios::out);
      for(long i = 0; i<dim; i++)
	{
	  kfile << i << "   " << kbins[i] << std::endl;
	}
      
    }

  // print kbins_all
  double max = kbins_all.max();
  kbins_all.mult(1.0/max);
  std::fstream kfile(((std::string)argv[1] + "-ana-kspec.dat").c_str(), std::ios::out);
  for(long i = 0; i<dim; i++)
    kfile << i << "   " << kbins_all[i] << std::endl;

  // close turb_file
  fturb.close();
  
  // clear fftw memory
  fftw_destroy_plan(p);
  fftw_free(in);
  fftw_free(out);

  return 0;
}
