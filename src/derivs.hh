// header file for numerical derivaties of 3D fields


// Philipp Girichidis, Apr 2011
// last change: 1 May 2011

#ifndef _DERIVS_HH_
#define _DERIVS_HH_

#include "vector-ndim.hh"

namespace derivs3D
{

  void dvdx(arr::v3D<double> v, arr::v3D<double> &dv, double h=1.0)
  {
    dv.assign(v.sizex(),v.sizey(),v.sizez(),0.0);
    for(std::size_t iz = 1; iz < v.sizez()-1; iz++)
      for(std::size_t iy = 1; iy < v.sizey()-1; iy++)
	for(std::size_t ix = 1; ix < v.sizex()-1; ix++)
	  dv(ix,iy,iz) = (v(ix+1,iy,iz)-v(ix-1,iy,iz))/(2.0*h);
  }
  
  void dvdy(arr::v3D<double> v, arr::v3D<double> &dv, double h=1.0)
  {
    dv.assign(v.sizex(),v.sizey(),v.sizez(),0.0);
    for(std::size_t iz = 1; iz < v.sizez()-1; iz++)
      for(std::size_t iy = 1; iy < v.sizey()-1; iy++)
	for(std::size_t ix = 1; ix < v.sizex()-1; ix++)
	  dv(ix,iy,iz) = (v(ix,iy+1,iz)-v(ix,iy-1,iz))/(2.0*h);
  }
  
  void dvdz(arr::v3D<double> v, arr::v3D<double> &dv, double h=1.0)
  {
    dv.assign(v.sizex(),v.sizey(),v.sizez(),0.0);
    for(std::size_t iz = 1; iz < v.sizez()-1; iz++)
      for(std::size_t iy = 1; iy < v.sizey()-1; iy++)
	for(std::size_t ix = 1; ix < v.sizex()-1; ix++)
	  dv(ix,iy,iz) = (v(ix,iy,iz+1)-v(ix,iy,iz-1))/(2.0*h);
  }

  void div(arr::v3D<double> vx, arr::v3D<double> vy, arr::v3D<double> vz, arr::v3D<double> &divv, double h=1.0)
  {
    divv.resize(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvxdx(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvydy(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvzdz(vx.sizex(),vx.sizey(),vx.sizez());
    dvdx(vx, dvxdx, h);
    dvdy(vy, dvydy, h);
    dvdz(vz, dvzdz, h);
    for(std::size_t iz = 1; iz < vx.sizez()-1; iz++)
      for(std::size_t iy = 1; iy < vx.sizey()-1; iy++)
	for(std::size_t ix = 1; ix < vx.sizex()-1; ix++)
	  divv(ix,iy,iz) = dvxdx(ix,iy,iz) + dvydy(ix,iy,iz) + dvzdz(ix,iy,iz);
  }
  
  void rot(arr::v3D<double> vx,    arr::v3D<double> vy,    arr::v3D<double> vz,
	   arr::v3D<double> &rotx, arr::v3D<double> &roty, arr::v3D<double> &rotz, double h=1.0)
  {
    rotx.resize(vx.sizex(),vx.sizey(),vx.sizez());
    roty.resize(vx.sizex(),vx.sizey(),vx.sizez());
    rotz.resize(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvxdy(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvxdz(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvydx(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvydz(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvzdx(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvzdy(vx.sizex(),vx.sizey(),vx.sizez());
    dvdy(vx, dvxdy, h);
    dvdz(vx, dvxdz, h);
    dvdx(vy, dvydx, h);
    dvdz(vy, dvydz, h);
    dvdx(vz, dvzdx, h);
    dvdy(vz, dvzdy, h);
    for(std::size_t iz = 1; iz < vx.sizez()-1; iz++)
      for(std::size_t iy = 1; iy < vx.sizey()-1; iy++)
	for(std::size_t ix = 1; ix < vx.sizex()-1; ix++)
	  {
	    rotx(ix,iy,iz) = dvzdy(ix,iy,iz) - dvydz(ix,iy,iz);
	    roty(ix,iy,iz) = dvxdz(ix,iy,iz) - dvzdx(ix,iy,iz);
	    rotz(ix,iy,iz) = dvydx(ix,iy,iz) - dvxdy(ix,iy,iz);
	  }
  }

}

namespace derivs3Dperiodic
{

  void dvdx(arr::v3D<double> v, arr::v3D<double> &dv, double h=1.0)
  {
    dv.assign(v.sizex(),v.sizey(),v.sizez(),0.0);
    for(std::size_t iz = 0; iz < v.sizez(); iz++)
      for(std::size_t iy = 0; iy < v.sizey(); iy++)
	for(std::size_t ix = 0; ix < v.sizex(); ix++)
	  dv(ix,iy,iz) = (v(ix+1,iy,iz)-v(ix-1,iy,iz))/(2.0*h);
  }
  
  void dvdy(arr::v3D<double> v, arr::v3D<double> &dv, double h=1.0)
  {
    dv.assign(v.sizex(),v.sizey(),v.sizez(),0.0);
    for(std::size_t iz = 0; iz < v.sizez(); iz++)
      for(std::size_t iy = 0; iy < v.sizey(); iy++)
	for(std::size_t ix = 0; ix < v.sizex(); ix++)
	  dv(ix,iy,iz) = (v(ix,iy+1,iz)-v(ix,iy-1,iz))/(2.0*h);
  }
  
  void dvdz(arr::v3D<double> v, arr::v3D<double> &dv, double h=1.0)
  {
    dv.assign(v.sizex(),v.sizey(),v.sizez(),0.0);
    for(std::size_t iz = 0; iz < v.sizez(); iz++)
      for(std::size_t iy = 0; iy < v.sizey(); iy++)
	for(std::size_t ix = 0; ix < v.sizex(); ix++)
	  dv(ix,iy,iz) = (v(ix,iy,iz+1)-v(ix,iy,iz-1))/(2.0*h);
  }

  void div(arr::v3D<double> vx, arr::v3D<double> vy, arr::v3D<double> vz, arr::v3D<double> &divv, double h=1.0)
  {
    divv.resize(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvxdx(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvydy(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvzdz(vx.sizex(),vx.sizey(),vx.sizez());
    dvdx(vx, dvxdx, h);
    dvdy(vy, dvydy, h);
    dvdz(vz, dvzdz, h);
    for(std::size_t iz = 0; iz < vx.sizez(); iz++)
      for(std::size_t iy = 0; iy < vx.sizey(); iy++)
	for(std::size_t ix = 0; ix < vx.sizex(); ix++)
	  divv(ix,iy,iz) = dvxdx(ix,iy,iz) + dvydy(ix,iy,iz) + dvzdz(ix,iy,iz);
  }
  
  void rot(arr::v3D<double> vx,    arr::v3D<double> vy,    arr::v3D<double> vz,
	   arr::v3D<double> &rotx, arr::v3D<double> &roty, arr::v3D<double> &rotz, double h=1.0)
  {
    rotx.resize(vx.sizex(),vx.sizey(),vx.sizez());
    roty.resize(vx.sizex(),vx.sizey(),vx.sizez());
    rotz.resize(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvxdy(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvxdz(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvydx(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvydz(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvzdx(vx.sizex(),vx.sizey(),vx.sizez());
    arr::v3D<double> dvzdy(vx.sizex(),vx.sizey(),vx.sizez());
    dvdy(vx, dvxdy, h);
    dvdz(vx, dvxdz, h);
    dvdx(vy, dvydx, h);
    dvdz(vy, dvydz, h);
    dvdx(vz, dvzdx, h);
    dvdy(vz, dvzdy, h);
    for(std::size_t iz = 0; iz < vx.sizez(); iz++)
      for(std::size_t iy = 0; iy < vx.sizey(); iy++)
	for(std::size_t ix = 0; ix < vx.sizex(); ix++)
	  {
	    rotx(ix,iy,iz) = dvzdy(ix,iy,iz) - dvydz(ix,iy,iz);
	    roty(ix,iy,iz) = dvxdz(ix,iy,iz) - dvzdx(ix,iy,iz);
	    rotz(ix,iy,iz) = dvydx(ix,iy,iz) - dvxdy(ix,iy,iz);
	  }
  }

}


#endif
