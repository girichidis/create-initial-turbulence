// create turbulence velocity field
// Philipp Girichidis, Nov 2010
// last change: 15 Aug 2011

#include <iostream>
#include <fstream>
#include "vector-ndim.hh"
#include <cmath>
#include <complex>
#include <fftw3.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "derivs.hh"
#include "create-turbulence.hh"

#ifdef CREATE_TURB_HDF_OUTPUT
#include <hdf5.h>
#include "HDFIO.h"
#endif

//#define CREATE_TURB_DEBUG

#ifdef CREATE_TURB_DEBUG
#include "histogram.h"
#endif

const double pi = 3.141592653589793238462643383279502;
const double SMALL = 1e-6;
const double TINY  = 1e-13;

//###############################
//
// HELPER FUNCTIONS
//
//###############################

void usage()
{
  std::cout << std::endl;
  std::cout << "USAGE / OPTIONS" << std::endl;
  std::cout << std::endl;
  std::cout << "  key=default-value" << std::endl;
  std::cout << "  switch [default]  T = true, F = false" << std::endl;
  std::cout << std::endl;
  std::cout << "  kmin=1         : k min" << std::endl;
  std::cout << "  kpeak=1        : k peak" << std::endl;
  std::cout << "  kmax=d/2-1     : k max" << std::endl;
  std::cout << "  sigma=1        : spread around random amplitude (only effect. if randamp)" << std::endl;
  std::cout << "  d=128          : number of data points per dimension" << std::endl;
  std::cout << "  p-lo=2         : power spectrum index below kpeak" << std::endl;
  std::cout << "  p-hi=-2        : power spectrum index above kpeak" << std::endl;
  std::cout << "  burgers [T]    : sets power spectrum index to burger turbulence = (p=-2)" << std::endl;
  std::cout << "  kolmogorov [F] : sets power spectrum index to kolmogorov turbulence = (p=-5/3)" << std::endl;
  std::cout << "  seed=0         : random seed" << std::endl;
  std::cout << "  o=vel3D        : output base file name" << std::endl;
  std::cout << "  flt/dbl [dbl]  : single (flt) / double (dbl) precision" << std::endl;
  std::cout << "  symm [F]       : symmetric velocity pattern" << std::endl;
  std::cout << "  norand [F]     : no random phase nor random amplitude" << std::endl;
  std::cout << "  randamp [F]    : random amplitude" << std::endl;
  std::cout << "  vnorm=1/sqrt(3): normalise velocity standard deviation" << std::endl;
  std::cout << "  noslices [F]   : do not print data files for slices" << std::endl;
  std::cout << "  nokslice [F]   : do not print data file for k-slice" << std::endl;
  std::cout << "  sol [F]        : do k-decomposition (solenoidal projection)" << std::endl;
  std::cout << "  comp [F]       : do k-decomposition (compressive projection)" << std::endl;
  std::cout << "  check [F]      : check power spectrum afterwards (undo FT)" << std::endl;
}

// returns the k direction vector shifted to (0,0,0) for FT
class k_direction
{
public:
  k_direction(std::size_t NN)
  {
    dim = NN;
  }
  inline double operator()(std::size_t i)
  {
    return (i < (dim+1)/2 ? (double)i : (double)i-(double)dim);
  }
  //inline double operator[](std::size_t i)
  //{
  //  return (i < (dim+1)/2 ? (double)i : fabs((double)i-(double)dim));
  //}
private:
  std::size_t dim;
};

double kmag(long x, long y, long z)
{
  return sqrt((double)(x*x + y*y + z*z));
}



//###############################
//
// MAIN PROGRAMME
//
//###############################

int main(int argc, char* argv[])
{

  std::cout << std::endl;
  std::cout << "  #########################################################" << std::endl;
  std::cout << "  #                                                       #" << std::endl;
  std::cout << "  #  CREATE TURBULENCE FIELD                              #" << std::endl;
  std::cout << "  #  Philipp Girichidis, 2010-2011                        #" << std::endl;
  std::cout << "  #  V3.0, August 2011                                    #" << std::endl;
  std::cout << "  #                                                       #" << std::endl;
  std::cout << "  #  - run with param 'help' for command line options     #" << std::endl;
  std::cout << "  #  - needs more memory                                  #" << std::endl;
  std::cout << "  #  - does projection into solenoidal/compressive modes  #" << std::endl;
  std::cout << "  #                                                       #" << std::endl;
  std::cout << "  #########################################################" << std::endl;
  std::cout << std::endl;

  // general simulation parameters
  size_t N                         = 7;
  long   dim                       = pow(2,N);
  double kmin                      = 1.0;
  // do not use kmax = dim/2-1 (Nyquist modes = 1/2 f_s)
  double kmax                      = (double)(dim/2-1);
  double kpeak                     = 1.0;
  double ps_exp_lo                 = 2.0;
  double ps_exp_hi                 = -2.0;
  double v_norm_stddev             = 1.0/sqrt(3.0);

  // random number parameters
  double ran_sigma                 = 1.0;
  unsigned long ran_seed           = 0;
  unsigned long ran_seed_offset[3] = {0,17,23};
  bool ran_symmetric               = false;
  bool random_phase                = true;
  bool random_amplitude            = false;

  // k decomposition
  bool k_decomposition             = false;
  bool k_decomposition_sol         = false;
  bool k_decomposition_comp        = false;

  // output & print parameters
  std::string outfile_base         = "vel3D";
  std::string output_type          = "double precision";
  bool print_k_slice               = true;
  bool print_v_slice               = true;
  std::string dim_str[3]           = {"x","y","z"};
  

  // check total energy and power spectrum
  double total_energy              = 0.0;
  bool   user_check_power          = false;

  // check wether help is needed
  if((argc > 1) && ((std::string)argv[1] == "help"))
    {
      usage();
      exit(0);
    }
  
  // analyse command line parameters
  for(int i=1; i<argc; i++)
    {
      std::string param = argv[i];
      if(param.substr(0,5) == "kmin=")
	{
	  kmin = atof((param.substr(5)).c_str());
	  std::cout << "  CHANGED kmin to " << kmin << std::endl;
	}
      if(param.substr(0,6) == "kpeak=")
	{
	  kpeak = atof((param.substr(6)).c_str());
	  std::cout << "  CHANGED kpeak to " << kpeak << std::endl;
	}
      if(param.substr(0,5) == "kmax=")
	{
	  kmax = atof((param.substr(5)).c_str());
	  if(kmax > dim/2-2)
	    {
	      std::cout << "  !! kmax must be smaller than dim/2-1 (no Nyquist frequencies)" << std::endl;
	      kmax = dim/2-1;
	    }
	  std::cout << "  CHANGED kmax to " << kmax << std::endl;
	  //std::cout << "    hint: if you change dim afterwards, kmax may be too large!" << std::endl;
	}
      if(param.substr(0,6) == "sigma=")
	{
	  ran_sigma = atof((param.substr(6)).c_str());
	  std::cout << "  CHANGED random spread to sigma = " << ran_sigma << std::endl;
	}
      if(param.substr(0,2) == "d=")
	{
	  dim = atoi((param.substr(2)).c_str());
	  std::cout << "  CHANGED dim to " << dim << std::endl;
	  // check that kmax is not larger than useful FT can handle
	  if(kmax>(double)(dim/2-1))
	    {
	      kmax = (double)(dim/2-1);
	      std::cout << "  ==> REDUCED kmax to " << kmax << std::endl;
	    }
	}
      if(param.substr(0,5) == "p-lo=")
	{
	  double p_lo = atof((param.substr(5)).c_str());
	  ps_exp_lo = p_lo/2.0 - 1.0;
	  std::cout << "  CHANGED power spectrum exponent below kpeak to P(k) ~ k^p, p = " << p_lo << std::endl;
	  std::cout << "                                                 v(k) ~ k^q, q = " << ps_exp_lo << std::endl;
	}
      if(param.substr(0,5) == "p-hi=")
	{
	  double p_hi = atof((param.substr(5)).c_str());
	  ps_exp_hi = p_hi/2.0 - 1.0;
	  std::cout << "  CHANGED power spectrum exponent above kpeak to P(k) ~ k^p, p = " << p_hi << std::endl;
	  std::cout << "                                                 v(k) ~ k^q, q = " << ps_exp_hi << std::endl;
	}
      if(param.substr(0,7) == "burgers")
	{
	  ps_exp_hi = (-2.0) / 2.0 - 1.0;
	  std::cout << "  CHANGED power spectrum Burgers spectrum, P(k) ~ k^p, p = -2 " << ps_exp_hi << std::endl;
	  std::cout << "                                           v(k) ~ k^q, q = -2" << std::endl;
	  std::cout << "                                           p = 2q+2 => q = p/2 - 1 = " << ps_exp_hi << std::endl;
	}
      if(param.substr(0,10) == "kolmogorov")
	{
	  ps_exp_hi = (-5.0/3.0) / 2.0 - 1.0;
	  std::cout << "  CHANGED power spectrum Kolmogorov spectrum, P(k) ~ k^p, p = -5/3 " << std::endl;
	  std::cout << "                                              v(k) ~ k^q, q = -11/6" << std::endl;
	  std::cout << "                                              p = 2q+2 => q = p/2 - 1 = -11/6 = " << ps_exp_hi << std::endl;
	}
      if(param.substr(0,5) == "seed=")
	{
	  ran_seed = atoi((param.substr(5)).c_str());
	  std::cout << "  CHANGED random seed to " << ran_seed << std::endl;
	}
      if(param.substr(0,2) == "o=")
	{
	  outfile_base = (param.substr(2)).c_str();
	  std::cout << "  CHANGED output file base name to " << outfile_base << std::endl;
	}
      if(param.substr(0,3) == "flt")
	{
	  output_type = "single precision";
	  std::cout << "  CHANGED output data type to float" << std::endl;
	}
      if(param.substr(0,3) == "dbl")
	{
	  output_type = "double precision";
	  std::cout << "  CHANGED output data type to double" << std::endl;
	}
      if(param.substr(0,4) == "symm")
	{
	  ran_symmetric = true;
	  std::cout << "  CHANGED to symmetric velocity field" << std::endl;
	}
      if(param.substr(0,6) == "norand")
	{
	  random_phase     = false;
	  random_amplitude = false;
	  std::cout << "  CHANGED to no random numbers" << std::endl;
	}
      if(param.substr(0,7) == "randamp")
	{
	  random_amplitude = true;
	  std::cout << "  CHANGED to random amplitude" << std::endl;
	}
      if(param.substr(0,6) == "vnorm=")
	{
	  v_norm_stddev = atof((param.substr(6)).c_str());
	  std::cout << "  CHANGED velocity std deviation normalisation v_norm = " << v_norm_stddev << std::endl;
	}
      if(param.substr(0,8) == "nokslice")
	{
	  print_k_slice = false;
	  std::cout << "  CHANGED: do not print k slice file!" << std::endl;
	}
      if(param.substr(0,8) == "novslice")
	{
	  print_v_slice = false;
	  std::cout << "  CHANGED: do not print velocity slice files!" << std::endl;
	}
      if(param.substr(0,8) == "noslices")
	{
	  print_v_slice = false;
	  print_k_slice = false;
	  std::cout << "  CHANGED: do not print any slice files!" << std::endl;
	}
      if(param.substr(0,4) == "comp")
	{
	  k_decomposition      = true;
	  k_decomposition_comp = true;
	  std::cout << "  DO K-DECOMPOSITION: compressive modes!" << std::endl;
	}
      if(param.substr(0,3) == "sol")
	{
	  k_decomposition     = true;
	  k_decomposition_sol = true;
	  std::cout << "  DO K-DECOMPOSITION: solenoidal modes!" << std::endl;
	}
      if(param.substr(0,5) == "check")
	{
	  user_check_power = true;
	  std::cout << "  CHECK power spectrum of the velocity field afterwards!" << std::endl;
	}
    }
  size_t Nelements = dim*dim*dim;
  std::cout << std::endl;


  // write information to info file
  std::cout << "  WRITING parameters to file " << outfile_base + ".info" << std::endl;
  std::fstream finfo((outfile_base + ".info").c_str(), std::ios::out);
  finfo << " command line call  : ";
  for(int i=0; i<argc; i++)
    finfo << argv[i] << " ";
  finfo << std::endl;
  finfo << " file basename      : " << outfile_base << std::endl;
  finfo << " data per direction : " << dim << std::endl;
  finfo << " total data per dir : " << Nelements << std::endl;
  finfo << " total data         : " << 3*Nelements << std::endl;
  finfo << " data type          : " << output_type << std::endl;
  finfo << " file size [MB]     : " << (output_type == "double precision" ?
					3*Nelements*sizeof(double)/1024/1024 :
					3*Nelements*sizeof(float)/1024/1024 ) << std::endl;
  finfo << " kmin               : " << kmin << std::endl;
  finfo << " kpeak              : " << kpeak << std::endl;
  finfo << " kmax               : " << kmax << std::endl;
  finfo << " powspec idx lo v(k): " << ps_exp_lo << std::endl;
  finfo << " powspec idx lo P(k): " << 2.0*(ps_exp_lo + 1.0) << std::endl;
  finfo << " powspec idx hi v(k): " << ps_exp_hi << std::endl;
  finfo << " powspec idx hi P(k): " << 2.0*(ps_exp_hi + 1.0) << std::endl;
  finfo << " init seed          : " << ran_seed << std::endl;
  finfo << " seed dimension 1   : " << ran_seed + ran_seed_offset[0] << std::endl;
  finfo << " seed dimension 2   : " << ran_seed + ran_seed_offset[1] << std::endl;
  finfo << " seed dimension 3   : " << ran_seed + ran_seed_offset[2] << std::endl;
  finfo << " symmetric field    : " << ran_symmetric << std::endl;
  finfo << " random phase       : " << random_phase << std::endl;
  finfo << " vel std dev norm   : " << v_norm_stddev << std::endl;
  finfo << " k-decomposition    : " << k_decomposition << std::endl;
  finfo << " k-decomp. solenoid : " << k_decomposition_sol << std::endl;
  finfo << " k-decomp. compress : " << k_decomposition_comp << std::endl;
  finfo.close();


  // prepare vectors
  arr::v3D<double> k3D(dim,0.0);
  arr::v3D<double> vel_r(dim);
  arr::v3D<double> vel_i(dim);
  arr::v3D<double> velx(dim);
  arr::v3D<double> vely(dim);
  arr::v3D<double> velz(dim);
  arr::v3D<double> divv(dim);
  arr::v3D<double> rotx(dim);
  arr::v3D<double> roty(dim);
  arr::v3D<double> rotz(dim);

  // create all arrays for k
  arr::v3D<double> kx_r(dim,0.0);
  arr::v3D<double> kx_i(dim,0.0);
  arr::v3D<double> ky_r(dim,0.0);
  arr::v3D<double> ky_i(dim,0.0);
  arr::v3D<double> kz_r(dim,0.0);
  arr::v3D<double> kz_i(dim,0.0);
  // longitudinal projection
  arr::v3D<double> kx_lgt_r;
  arr::v3D<double> kx_lgt_i;
  arr::v3D<double> ky_lgt_r;
  arr::v3D<double> ky_lgt_i;
  arr::v3D<double> kz_lgt_r;
  arr::v3D<double> kz_lgt_i;
  // only expand projection array if the projection is chosen
  if(k_decomposition_comp)
    {
      kx_lgt_r.assign3D(dim,0.0);
      kx_lgt_i.assign3D(dim,0.0);
      ky_lgt_r.assign3D(dim,0.0);
      ky_lgt_i.assign3D(dim,0.0);
      kz_lgt_r.assign3D(dim,0.0);
      kz_lgt_i.assign3D(dim,0.0);
    }
  // transversal projection
  arr::v3D<double> kx_trv_r;
  arr::v3D<double> kx_trv_i;
  arr::v3D<double> ky_trv_r;
  arr::v3D<double> ky_trv_i;
  arr::v3D<double> kz_trv_r;
  arr::v3D<double> kz_trv_i;
  // only expand projection array if the projection is chosen
  if(k_decomposition_sol)
    {
      kx_trv_r.assign3D(dim,0.0);
      kx_trv_i.assign3D(dim,0.0);
      ky_trv_r.assign3D(dim,0.0);
      ky_trv_i.assign3D(dim,0.0);
      kz_trv_r.assign3D(dim,0.0);
      kz_trv_i.assign3D(dim,0.0);
    }


  // set value according to power spectrum
  double kmin_test = 2.0*(double)dim; // just set to very large value
  double kmax_test = -1.0;
  long k_end = (dim/2 < 1+(long)kmax ? dim/2 : 1+(long)kmax);

  //#ifdef CREATE_TURB_DEBUG
  arr::v1D<double> k_bins(dim,0.0);
  arr::v1D<double> k_pow_bins(dim,0.0);
  //#endif

  // calculate k-vectors for one corner of the k cube
  // k-vectors are located like index numbers
  //   i.e. k_000 is at position [0][0][0]
  // up to half of the cube (Nyquist-k)
  // after that negative k parts
  //
  // storage picture
  // 
  //  - X = k_000
  // /
  // X 1 2 3 0 3 2 1
  // 1 2 3 4 0 4 3 2
  // 2 3 4 5 0 5 4 3
  // 3 4 5 6 0 6 5 4
  // 0 0 0 0 0 0 0 0 |Nyquist "row"
  // 3 4 5 6 0 6 5 4
  // 2 3 4 5 0 5 4 3
  // 1 2 3 4 0 4 3 2
  // k_pos  |N|k_neg
  //         \_Nyquist "column"

  for(long iz=0; iz<k_end; iz++)
    for(long iy=-k_end+1; iy<k_end; iy++)
      for(long ix=-k_end+1; ix<k_end; ix++)
	{
	  double length = sqrt(pow((double)ix,2.0)+
			       pow((double)iy,2.0)+
			       pow((double)iz,2.0));

	  if((length >= kmin-SMALL) && (length <= kmax+SMALL))
	    {
	      if(length > kmax_test) kmax_test = length;
 	      if(length < kmin_test) kmin_test = length;
	      k3D(ix,iy,iz)    = power_spectrum_pl(kpeak,length,ps_exp_lo,ps_exp_hi);
	      k3D(-ix,-iy,-iz) = power_spectrum_pl(kpeak,length,ps_exp_lo,ps_exp_hi);
	    }
	  else
	    {
	      k3D(ix,iy,iz)    = 0.0;
	      k3D(-ix,-iy,-iz) = 0.0;
 	    }
	  //#ifdef CREATE_TURB_DEBUG
	  // find centre between two bins in log space
	  // upper value c, lower value f
	  // log_mid_pos = log(f) + 0.5*(log(c) - log(f))
	  //             = 0.5*(log(f)+log(c))
	  //             = 0.5*log(c*f)
	  // mid_pos = 10^log_mid_pos
	  double mid_pos = pow(10,0.5*log10(floor(length)*ceil(length)));
	  long   bin_pos = (length < mid_pos ? (long)floor(length) : (long)ceil(length));
	  k_bins    [bin_pos] += 2.0*k3D(ix,iy,iz);
	  k_pow_bins[bin_pos] += 2.0*pow(k3D(ix,iy,iz),2.0);
	  //#endif

	}
  std::cout << "  CHECKED kmin / kmax : [" << kmin_test << ";" << kmax_test << "]" << std::endl;


  //#ifdef CREATE_TURB_DEBUG
  std::cout << "  DEBUG: WRITING initial k spectrum to file \"" 
	    << outfile_base + "-kspec.dat\"" << std::endl;
  std::fstream kspec_file((outfile_base + "-kspec.dat").c_str(), std::ios::out);
  kspec_file << "#             i         abs(k)            k^2" << std::endl;
  double k_bins_max     = k_bins.max();
  double k_pow_bins_max = k_pow_bins.max();
  for(unsigned long i=0; i<k_bins.size(); i++)
    {
      kspec_file << std::setw(15) << i
		 << std::setw(15) << k_bins[i]/k_bins_max
		 << std::setw(15) << k_pow_bins[i]/k_pow_bins_max
		 << std::endl;
    }
  kspec_file.close();
  //#endif

  // Fourier transform
  // using fftw library
  // prepare fftw data arrays and fftw plan
  fftw_complex *in, *out;
  fftw_plan p;
  in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * k3D.size());
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * k3D.size());
  p   = fftw_plan_dft_3d(dim, dim, dim, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    

  // prepare data arrays
  arr::v3D<double> amplitude_r(dim,0.0);
  arr::v3D<double> amplitude_i(dim,0.0);
  arr::v3D<double> phase(dim,0.0);
  arr::v3D<double> ampli(dim,1.0);


  // prepare output file
  std::fstream fout((outfile_base + ".bin").c_str(), std::ios::out | std::ios::binary);
  std::cout << "  WRITING data to file " << outfile_base + ".bin" << std::endl;


  // prepare three transforms with different seeds for three velocities velx, vely, velz
  for(unsigned int dimension = 0; dimension < 3; dimension++)
    {
      std::cout << std::endl;
      std::cout << "  PREPARING velocity field for dimension " << dimension+1 
		<< " with seed " << ran_seed + ran_seed_offset[dimension] << std::endl;

      // fill k-data into input array, including random numbers for phases and amplitudes

      if(random_phase)
	{
	  // create random numbers
	  random_uniform3D(dim, phase, ran_seed + ran_seed_offset[dimension], 2.0*pi);
	  for(long iz=0; iz<k_end; iz++)
	    for(long iy=-k_end; iy<k_end; iy++)
	      for(long ix=-k_end; ix<k_end; ix++)
		phase(-ix,-iy,-iz) = phase(ix,iy,iz);
	}

      if(random_amplitude)
	{
	  // generate random amplitude
	  random_gaussian3D(dim, ampli, ran_seed + ran_seed_offset[dimension], ran_sigma);
	  for(long iz=0; iz<k_end; iz++)
	    for(long iy=-k_end; iy<k_end; iy++)
	      for(long ix=-k_end; ix<k_end; ix++)
		ampli(-ix,-iy,-iz) = ampli(ix,iy,iz);
	}


      // if symmetric, then mirror the coefficients in center of array
      if(ran_symmetric)
	{
	  ampli.mirrorX(dim/2.0);
	  ampli.mirrorY(dim/2.0);
	  ampli.mirrorZ(dim/2.0);
	  phase.mirrorX(dim/2.0);
	  phase.mirrorY(dim/2.0);
	  phase.mirrorZ(dim/2.0);
	}


      arr::mult(amplitude_r,ampli,k3D);

      // set imaginary amplitude != 0, if NOT symmetric
      if(!ran_symmetric)
	{
	  for(long iz=-k_end; iz<0; iz++)
	    for(long iy=-k_end; iy<k_end; iy++)
	      for(long ix=-k_end; ix<k_end; ix++)
		amplitude_i(ix,iy,iz) = -1.0*ampli(ix,iy,iz)*k3D(ix,iy,iz);
	  
	  // setting haf of the iz=0 plane to negative values needs more loops
	  for(long iy=-k_end; iy<k_end; iy++)
	    for(long ix=-k_end; ix<k_end; ix++)
	      {
		if(iy<0)
		  amplitude_i(ix,iy,0) = -1.0*ampli(ix,iy,0)*k3D(ix,iy,0);
		else
		  amplitude_i(ix,iy,0) = ampli(ix,iy,0)*k3D(ix,iy,0);
	      }
	  // ... and half of the values along x 
	  for(long ix=k_end; ix<dim; ix++)
	    amplitude_i(ix,0,0) = -amplitude_i(ix,0,0);
	  
	  for(long iz=1; iz<k_end; iz++)
	    for(long iy=-k_end; iy<k_end; iy++)
	      for(long ix=-k_end; ix<k_end; ix++)
		amplitude_i(ix,iy,iz) = ampli(ix,iy,iz)*k3D(ix,iy,iz);
	}


      if(dimension == 0)
	for(std::size_t i = 0; i<k3D.size(); i++)
	  {
	    kx_r[i] = amplitude_r[i] * cos(phase[i]);
	    kx_i[i] = amplitude_i[i] * sin(phase[i]);
	  }
      if(dimension == 1)
	for(std::size_t i = 0; i<k3D.size(); i++)
	  {
	    ky_r[i] = amplitude_r[i] * cos(phase[i]);
	    ky_i[i] = amplitude_i[i] * sin(phase[i]);
	  }
      if(dimension == 2)
	for(std::size_t i = 0; i<k3D.size(); i++)
	  {
	    kz_r[i] = amplitude_r[i] * cos(phase[i]);
	    kz_i[i] = amplitude_i[i] * sin(phase[i]);
	  }
    }


  // do decomposition
  if(k_decomposition)
    {
      std::cout << std::endl;
      std::cout << "  DECOMPOSE K FIELDS " << std::endl;
      
      // create k-direction object
      k_direction k(dim);

      if(k_decomposition_comp)
	{
	  std::cout << "    compressive modes" << std::endl;
	  for(long z=0; z<dim; z++)
	    for(long y=0; y<dim; y++)
	      for(long x=0; x<dim; x++)
		{
		  // longitudinal projection
		  //=========================
		  
		  double km2 = pow(kmag(k(x),k(y),k(z)),2.0);

		  double qr = (k(x)*kx_r(x,y,z) + k(y)*ky_r(x,y,z) + k(z)*kz_r(x,y,z));
		  double qi = (k(x)*kx_i(x,y,z) + k(y)*ky_i(x,y,z) + k(z)*kz_i(x,y,z));
		  if(km2 > TINY)
		    {
		      qr /= km2;
		      qi /= km2;
		    }
		  
		  // real part
		  kx_lgt_r(x,y,z) = k(x)*qr;
		  ky_lgt_r(x,y,z) = k(y)*qr;
		  kz_lgt_r(x,y,z) = k(z)*qr;
		  
		  // imaginary part
		  kx_lgt_i(x,y,z) = k(x)*qi;
		  ky_lgt_i(x,y,z) = k(y)*qi;
		  kz_lgt_i(x,y,z) = k(z)*qi;

		}
	}

      
      if(k_decomposition_sol)
	{
	  std::cout << "    solenoidal modes" << std::endl;
	  for(long z=0; z<dim; z++)
	    for(long y=0; y<dim; y++)
	      for(long x=0; x<dim; x++)
		{
		  // transversal projection
		  //=========================
		  
		  double km2 = pow(kmag(k(x),k(y),k(z)),2.0);

		  double qr = (k(x)*kx_r(x,y,z) + k(y)*ky_r(x,y,z) + k(z)*kz_r(x,y,z));
		  double qi = (k(x)*kx_i(x,y,z) + k(y)*ky_i(x,y,z) + k(z)*kz_i(x,y,z));
		  if(km2 > TINY)
		    {
		      qr /= km2;
		      qi /= km2;
		    }

		  // real part
		  kx_trv_r(x,y,z) = kx_r(x,y,z) - k(x)*qr;
		  ky_trv_r(x,y,z) = ky_r(x,y,z) - k(y)*qr;
		  kz_trv_r(x,y,z) = kz_r(x,y,z) - k(z)*qr;
		  
		  // imaginary part
		  kx_trv_i(x,y,z) = kx_i(x,y,z) - k(x)*qi;
		  ky_trv_i(x,y,z) = ky_i(x,y,z) - k(y)*qi;
		  kz_trv_i(x,y,z) = kz_i(x,y,z) - k(z)*qi;
		}
	}
    }
  
  
  // create velocities velx, vely, velz
  for(unsigned int dimension = 0; dimension < 3; dimension++)
    {
      std::cout << std::endl;
      std::cout << "  CREATING velocity field for dimension " << dimension+1 
		<< " with seed " << ran_seed + ran_seed_offset[dimension] << std::endl;

      if(dimension == 0)
	{
	  if(k_decomposition_comp)
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = kx_lgt_r[i];
		in[i][1] = kx_lgt_i[i];
	      }
	  else if(k_decomposition_sol)
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = kx_trv_r[i];
		in[i][1] = kx_trv_i[i];
	      }
	  else
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = kx_r[i];
		in[i][1] = kx_i[i];
	      }
	}
      if(dimension == 1)
	{
	  if(k_decomposition_comp)
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = ky_lgt_r[i];
		in[i][1] = ky_lgt_i[i];
	      }
	  else if(k_decomposition_sol)
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = ky_trv_r[i];
		in[i][1] = ky_trv_i[i];
	      }
	  else
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = ky_r[i];
		in[i][1] = ky_i[i];
	      }
	}
      if(dimension == 2)
	{
	  if(k_decomposition_comp)
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = kz_lgt_r[i];
		in[i][1] = kz_lgt_i[i];
	      }
	  else if(k_decomposition_sol)
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = kz_trv_r[i];
		in[i][1] = kz_trv_i[i];
	      }
	  else
	    for(std::size_t i = 0; i<k3D.size(); i++)
	      {
		in[i][0] = kz_r[i];
		in[i][1] = kz_i[i];
	      }
	}

      
      // do fourier transformation
      fftw_execute(p);
      
      
      // get output data in more comfortable array
      for(std::size_t i = 0; i<k3D.size(); i++)
	{
	  vel_r(i) = out[i][0];
	  vel_i(i) = out[i][1];
	}


      // analyse fourier transformed field and normalise it
      double minr, maxr, mini, maxi, stddevr, meanr, skewr;
      vel_r.minmax(minr, maxr);
      vel_i.minmax(mini, maxi);
      arr::statist_moments(vel_r, meanr, stddevr, skewr);
      std::cout << "  VEL MIN/MAX real : " << minr << " " << maxr << std::endl;
      std::cout << "              imag : " << mini << " " << maxi << std::endl;
      std::cout << "              mean : " << meanr << std::endl;
      std::cout << "            stddev : " << stddevr << std::endl;
      std::cout << "          skewness : " << skewr << std::endl;
      
#ifdef CREATE_TURB_DEBUG
      histogram HST;
      HST.set(&vel_r.front(),vel_r.size(),60);
      HST.plot();
#endif

      // normalise velocities and check energy
      vel_r.mult(v_norm_stddev/stddevr);
      stddevr = arr::stddev(vel_r, meanr);
      vel_r.minmax(minr, maxr);
      std::cout << "  NEW  min/max     : " << minr << " " << maxr << std::endl;
      std::cout << "  NEW  mean/stddev : " << meanr << " " << stddevr << std::endl;
      for(std::size_t i = 0; i<k3D.size(); i++)
	total_energy += 0.5*vel_r(i)*vel_r(i)/Nelements;

      
      // write data to binary output file
      if(output_type == "double precision")
	{
	  std::size_t SIZE = sizeof(vel_r(0));
	  for(std::size_t i=0; i<k3D.size(); i++)
	    fout.write((char*) &vel_r(i), SIZE);
	}
      else
	{
	  float dummy;
	  std::size_t SIZE = sizeof(dummy);
	  for(std::size_t i=0; i<k3D.size(); i++)
	    {
	      dummy = (float)vel_r(i);
	      fout.write((char*) &dummy, SIZE);
	    }
	}


      // store velocity in separate variable for later check
      // of decomposition
      if(dimension == 0)
	for(std::size_t i = 0; i<k3D.size(); i++)
	  velx(i) = vel_r(i);
      if(dimension == 1)
	for(std::size_t i = 0; i<k3D.size(); i++)
	  vely(i) = vel_r(i);
      if(dimension == 2)
	for(std::size_t i = 0; i<k3D.size(); i++)
	  velz(i) = vel_r(i);



      // write HDF5 output
#ifdef CREATE_TURB_HDF_OUTPUT 
      HDFIO HDFOutput = HDFIO();
      std::string hdf5filename;
      std::string varname = "vel" + dim_str[dimension];
      hdf5filename = outfile_base + "_" + varname;
      std::cout << std::endl;
      std::cout << "  WRITING hdf5 file " << hdf5filename << std::endl;
      HDFOutput.create(hdf5filename);
      /// write data
      std::vector<int> hdf5dims(3);
      hdf5dims[2] = dim;
      hdf5dims[1] = dim;
      hdf5dims[0] = dim;
      float * flaotfield = new float[vel_r.size()];
      for (unsigned long n = 0; n < vel_r.size(); n++) flaotfield[n] = vel_r[n];
      HDFOutput.write(flaotfield, varname, hdf5dims, H5T_NATIVE_FLOAT);
      HDFOutput.close();
      delete [] flaotfield;
#endif
      
    }

  // close output file
  fout.close();


  // write HDF5 dummy density for Christoph's tool
#ifdef CREATE_TURB_HDF_OUTPUT 
  HDFIO HDFOutput = HDFIO();
  std::string hdf5filename;
  std::string varname = "dens";
  hdf5filename = outfile_base + "_" + varname;
  std::cout << std::endl;
  std::cout << "  WRITING hdf5 file " << hdf5filename << std::endl;
  HDFOutput.create(hdf5filename);
  /// write data
  std::vector<int> hdf5dims(3);
  hdf5dims[2] = dim;
  hdf5dims[1] = dim;
  hdf5dims[0] = dim;
  float * flaotfield = new float[vel_r.size()];
  for (unsigned long n = 0; n < vel_r.size(); n++) flaotfield[n] = 1.0;
  HDFOutput.write(flaotfield, varname, hdf5dims, H5T_NATIVE_FLOAT);
  HDFOutput.close();
  delete [] flaotfield;
#endif

  //==========================================
  // DONE WITH FOURIER TRANSFORM
  //==========================================

  
  // check energy
  std::cout << std::endl
	    << "  CHECK total energy density 0.5*|v|^2 = " << total_energy << std::endl;
  

  //==========================================
  // CHECK COMPOSITION OF MODES 
  //==========================================

  if(true) //k_decomposition)
    {
      std::cout << std::endl;
      std::cout << "  CHECK curl(v) and div(v) in real space" << std::endl;
      derivs3Dperiodic::div(velx,vely,velz,divv);
      derivs3Dperiodic::rot(velx,vely,velz,rotx,roty,rotz);
      
      double rms_divv = sqrt(arr::mean(arr::sqr(divv)));
      double rms_rotv = sqrt(arr::mean(arr::norm2(rotx,roty,rotz)));

      std::cout << "    rms(div(v))  (a) = " << rms_divv << std::endl;
      std::cout << "    rms(rot(v))  (b) = " << rms_rotv << std::endl;
      std::cout << "    ratio (a)/(b)    = " << rms_divv/rms_rotv << std::endl;
      std::cout << std::endl;
    }


  //==========================================
  // PRINT 2D CONTROL SLICES    
  //==========================================

  if(print_k_slice)
    {
      // get first slice and store in file
      std::cout << "  WRITING k slice 1 to file \"" 
		<< outfile_base + "-kslice.dat" << "\"" << std::endl;
      arr::v2D<double> kslice(dim);
      k3D.get_sheet2(1,kslice);
      std::fstream kfile((outfile_base + "-kslice.dat").c_str(), std::ios::out);
      kslice.print_dat_form(kfile);
      kfile.close();
    }
  if(print_v_slice)
    {
      arr::v2D<double> vslice(dim);
      std::fstream vfile;

      // X slice
      std::cout << "  WRITING vel_x slice dim/2 to file \"" 
		<< outfile_base + "-vsliceX.dat" << "\"" << std::endl;
      vel_r.get_sheetX2(dim/2,vslice);
      vfile.open((outfile_base + "-vsliceX.dat").c_str(),std::ios::out);
      vslice.print_dat_form(vfile);
      vfile.close();

      // Y slice
      std::cout << "  WRITING vel_y slice dim/2 to file \"" 
		<< outfile_base + "-vsliceY.dat" << "\"" << std::endl;
      vel_r.get_sheetY2(dim/2,vslice);
      vfile.open((outfile_base + "-vsliceY.dat").c_str(),std::ios::out);
      vslice.print_dat_form(vfile);
      vfile.close();
      
      // Yslice
      std::cout << "  WRITING vel_z slice dim/2 to file \"" 
		<< outfile_base + "-vsliceZ.dat" << "\"" << std::endl;
      vel_r.get_sheetZ2(dim/2,vslice);
      vfile.open((outfile_base + "-vsliceZ.dat").c_str(),std::ios::out);
      vslice.print_dat_form(vfile);
      vfile.close();
    }


  //==========================================
  // CHECK POWER SPECTRUM    
  //==========================================

  if(user_check_power)
    {
      std::cout << "  CHECK power spectrum: do inverse fourier-transform!" << std::endl;

      p = fftw_plan_dft_3d(dim, dim, dim, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
      arr::v1D<double> k_spec_check(dim,0.0);
      arr::v1D<double> k_pow_spec_check(dim,0.0);
      for(int dimension = 0; dimension < 3; dimension++)
	{

	  if(dimension==0)
	    for(unsigned long i=0; i<k3D.size(); i++)
	      {
		in[i][0] = velx(i);
		in[i][1] = 0.0;
	      }
	  if(dimension==1)
	    for(unsigned long i=0; i<k3D.size(); i++)
	      {
		in[i][0] = vely(i);
		in[i][1] = 0.0;
	      }
	  if(dimension==2)
	    for(unsigned long i=0; i<k3D.size(); i++)
	      {
		in[i][0] = velz(i);
		in[i][1] = 0.0;
	      }
	  fftw_execute(p);
	  for(long iz=-dim/2+1; iz<dim/2; iz++)
	    for(long iy=-dim/2+1; iy<dim/2; iy++)
	      for(long ix=-dim/2+1; ix<dim/2; ix++)
		{
		  long ipos = (long)round(kmag(ix,iy,iz));
		  long ii   = (((iz+dim)%dim)*dim*dim + 
			       ((iy+dim)%dim)*dim +
			       (ix+dim)%dim);
		  k_spec_check    [ipos] += fabs(out[ii][0]);
		  k_pow_spec_check[ipos] += pow(out[ii][0],2.0);
		}
	}
      double k_spec_max     = k_spec_check.max();
      double k_pow_spec_max = k_pow_spec_check.max();
      std::fstream file((outfile_base + "-kspec1.dat").c_str(), std::ios::out);
      file << "#             i         abs(k)            k^2" << std::endl;
      for(long i=0; i< dim; i++)
	file << std::setw(15) << i
	     << std::setw(15) << k_spec_check[i]/k_spec_max
	     << std::setw(15) << k_pow_spec_check[i]/k_pow_spec_max << std::endl;
      file.close();
    }


  // clear fftw memory
  fftw_destroy_plan(p);
  fftw_free(in);
  fftw_free(out);

  return 0;
}
