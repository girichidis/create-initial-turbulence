// header file with auxiliary functions used in both V2.0 and V3.0
// Philipp Girichidis, July 2011

// last change: 22 July 2011


#ifndef _CREATE_TURBULENCE_HH_
#define _CREATE_TURBULENCE_HH_

double power_spectrum_pl(double kpeak, double k, double exponent1, double exponent2)
{
  if(k<1e-6) return 0.0;
  else
    {
      if(k <= kpeak) return pow(k/kpeak,exponent1);
      else return pow(k/kpeak,exponent2);
    }
}

void random_uniform(std::size_t N, std::vector<double> &data, unsigned long int seed=0, double factor=1.0)
{
  data.resize(N);

  gsl_rng * r;
  const gsl_rng_type * T;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
  gsl_rng_set(r,seed);

  for(std::size_t i = 0; i<N; i++)
    {
      data[i] = factor*gsl_rng_uniform_pos(r);
    }
  gsl_rng_free(r);
}

void random_gaussian(std::size_t N, std::vector<double> &data, unsigned long int seed=0, double sigma=1.0)
{
  data.resize(N);

  gsl_rng * r;
  const gsl_rng_type * T;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
  gsl_rng_set(r,seed);

  for(std::size_t i = 0; i<N; i++)
    {
      data[i] = gsl_ran_gaussian(r,sigma);
    }
  gsl_rng_free(r);
}

void random_uniform3D(std::size_t dim, std::vector<double> &data, unsigned long int seed=0, double factor=1.0)
{
  random_uniform(dim*dim*dim, data, seed, factor);
}

void random_gaussian3D(std::size_t dim, std::vector<double> &data, unsigned long int seed=0, double sigma=1.0)
{
  random_gaussian(dim*dim*dim, data, seed);
}


#endif
