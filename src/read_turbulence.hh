// header file for reading turbulence box
// with functions to interpolate


// Philipp Girichidis, Nov 2010
// last change: 05 Nov 2010

#ifndef _READ_TURBULENCE_HH_
#define _READ_TURBULENCE_HH_

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <cstdlib>

namespace turb
{
  class turbfile
  {
  public:
    turbfile(std::string FNAME, int DIM,
	     double x0=0.0, double x1=1.0,
	     double y0=0.0, double y1=1.0,
	     double z0=0.0, double z1=1.0)
    {
      fname = FNAME;
      dim = DIM;
      NN = dim*dim*dim;
      read_file(FNAME);
      set_bounds(x0, x1, y0, y1, z0, z1);
    }
    
    void set_bounds(std::vector<double> bnds, bool verbose = false)
    {
      if(bnds.size() < 6)
	{
	  std::cout << " \"bdns\" vector not large enough in turbfile::set_bounds() " << std::endl;
	  exit(1);
	}
      set_bounds(bnds[0], bnds[1], bnds[2], bnds[3], bnds[4], bnds[5], verbose);
    }

    void set_bounds(double x0_, double x1_, double y0_, double y1_, double z0_, double z1_, bool verbose = false)
    {
      x0 = x0_;   
      x1 = x1_;
      y0 = y0_;
      y1 = y1_;
      z0 = z0_;
      z1 = z1_;
      dx = (x1-x0)/(double)(dim-1);
      dy = (y1-y0)/(double)(dim-1);
      dz = (z1-z0)/(double)(dim-1);
      if(verbose)
	{
	  std::cout << " boundaries set to " << std::endl;
	  std::cout << " [" << x0 << ";" << x1 << "]" << std::endl;
	  std::cout << " [" << y0 << ";" << y1 << "]" << std::endl;
	  std::cout << " [" << z0 << ";" << z1 << "]" << std::endl;
	}
    }

    void read_file(std::string fname)
    {
      // set up data fields
      velx = new float[NN];
      vely = new float[NN];
      velz = new float[NN];

      // Read data from file
      std::fstream file(fname.c_str(),std::ios::in | std::ios::binary);
      int SIZE = sizeof(velx[0]);
      for(int i=0; i<NN; i++)
	file.read(reinterpret_cast<char *>(&velx[i]), SIZE);
      for(int i=0; i<NN; i++)
	file.read(reinterpret_cast<char *>(&vely[i]), SIZE);
      for(int i=0; i<NN; i++)
	file.read(reinterpret_cast<char *>(&velz[i]), SIZE);
      file.close();
    }
    
    ~turbfile()
    {
      delete[] velx;
      delete[] vely;
      delete[] velz;
    }

    bool check_bounds(int ix, int iy, int iz, bool quiet = false)
    {
      if((ix < 0) || (ix > dim-1) ||
	 (iy < 0) || (iy > dim-1) ||
	 (iz < 0) || (iz > dim-1))
	{
	  std::cout << " index out of range in \"turbfile::check_bounds()\"" << std::endl;
	  if(quiet) return false;
	  else exit(1);
	}
      return true;
    }

    double get_abs_value(int ix, int iy, int iz)
    {
      check_bounds(ix,iy,iz);
      return sqrt(pow(velx[iz*dim*dim + iy*dim + ix],2) +
		  pow(vely[iz*dim*dim + iy*dim + ix],2) +
		  pow(velz[iz*dim*dim + iy*dim + ix],2));
    }

    double get_xvalue(int ix, int iy, int iz)
    {
      check_bounds(ix,iy,iz);
      return velx[iz*dim*dim + iy*dim + ix];
    }
    double get_yvalue(int ix, int iy, int iz)
    {
      check_bounds(ix,iy,iz);
      return vely[iz*dim*dim + iy*dim + ix];
    }
    double get_zvalue(int ix, int iy, int iz)
    {
      check_bounds(ix,iy,iz);
      return velz[iz*dim*dim + iy*dim + ix];
    }
    
    std::vector<double> get_value(int ix, int iy, int iz)
    {
      std::vector<double> temp(3,0.0);
      check_bounds(ix,iy,iz);
      temp[0] = velx[iz*dim*dim + iy*dim + ix];
      temp[1] = vely[iz*dim*dim + iy*dim + ix];
      temp[2] = velz[iz*dim*dim + iy*dim + ix];
      return temp;
    }

    void interpolate(double x, double y, double z, double &vx, double &vy, double &vz)
    {
      std::vector<double> temp = interpolate(x,y,z);
      vx = temp[0];
      vy = temp[1];
      vz = temp[2];
    }
    
    std::vector<double> interpolate(std::vector<double> pos)
    {
      if(pos.size() < 3)
	{
	  std::cout << " \"pos\" vector not large enough in turbfile::interpolate() " << std::endl;
	  exit(1);
	}
      return interpolate(pos[0], pos[1], pos[2]);
    }

    std::vector<double> interpolate(double x, double y, double z)
    {
      if((x < x0) || (x >= x1) ||
	 (y < y0) || (y >= y1) ||
	 (z < z0) || (z >= z1))
	{
	  std::cout << "interpolation coordinate is outside of the box!" << std::endl;
	  std::cout << "x=" << x << " in [" << x0 << ";" << x1 << "] ?" << std::endl;
	  std::cout << "y=" << y << " in [" << y0 << ";" << y1 << "] ?" << std::endl;
	  std::cout << "z=" << z << " in [" << z0 << ";" << z1 << "] ?" << std::endl;
	  exit(1);
	}
      std::vector<double> temp(3,0.0);

      // determine cell indices
      int ix = (int)((x-x0)/dx);
      int iy = (int)((y-y0)/dy);
      int iz = (int)((z-z0)/dz);
      int ixx = ix++;
      int iyy = iy++;
      int izz = iz++;
      check_bounds(ix,iy,iz);
      check_bounds(ixx,iyy,izz);
      
      double xd = x/dx - (double)ix;
      double yd = y/dy - (double)iy;
      double zd = z/dz - (double)iz;

      double i1,i2,j1,j2,w1,w2;

      // interpolate all three values
      
      // X
      
      // First we interpolate along z(imagine we are pushing the front face of the cube to the back), giving
      i1 = velx[iz*dim*dim + iy *dim + ix ] * (1.0-zd) + velx[izz*dim*dim + iy* dim + ix ] * zd;
      i2 = velx[iz*dim*dim + iyy*dim + ix ] * (1.0-zd) + velx[izz*dim*dim + iyy*dim + ix ] * zd;
      j1 = velx[iz*dim*dim + iy *dim + ixx] * (1.0-zd) + velx[izz*dim*dim + iy *dim + ixx] * zd;
      j2 = velx[iz*dim*dim + iyy*dim + ixx] * (1.0-zd) + velx[izz*dim*dim + iyy*dim + ixx] * zd;
     
      // Then we interpolate these values (along y, as we were pushing the top edge to the bottom), giving
      w1 = i1*(1.0-yd) + i2*yd;
      w2 = j1*(1.0-yd) + j2*yd;

      // Finally we interpolate these values along x(walking through a line)
      temp[0] = w1*(1.0-xd) + w2*xd;


      // Y
      
      // First we interpolate along z(imagine we are pushing the front face of the cube to the back), giving
      i1 = vely[iz*dim*dim + iy *dim + ix ] * (1.0-zd) + vely[izz*dim*dim + iy* dim + ix ] * zd;
      i2 = vely[iz*dim*dim + iyy*dim + ix ] * (1.0-zd) + vely[izz*dim*dim + iyy*dim + ix ] * zd;
      j1 = vely[iz*dim*dim + iy *dim + ixx] * (1.0-zd) + vely[izz*dim*dim + iy *dim + ixx] * zd;
      j2 = vely[iz*dim*dim + iyy*dim + ixx] * (1.0-zd) + vely[izz*dim*dim + iyy*dim + ixx] * zd;
     
      w1 = i1*(1.0-yd) + i2*yd;
      w2 = j1*(1.0-yd) + j2*yd;

      temp[1] = w1*(1.0-xd) + w2*xd;


      // Z
      
      i1 = velz[iz*dim*dim + iy *dim + ix ] * (1.0-zd) + velz[izz*dim*dim + iy* dim + ix ] * zd;
      i2 = velz[iz*dim*dim + iyy*dim + ix ] * (1.0-zd) + velz[izz*dim*dim + iyy*dim + ix ] * zd;
      j1 = velz[iz*dim*dim + iy *dim + ixx] * (1.0-zd) + velz[izz*dim*dim + iy *dim + ixx] * zd;
      j2 = velz[iz*dim*dim + iyy*dim + ixx] * (1.0-zd) + velz[izz*dim*dim + iyy*dim + ixx] * zd;
     
      w1 = i1*(1.0-yd) + i2*yd;
      w2 = j1*(1.0-yd) + j2*yd;

      temp[2] = w1*(1.0-xd) + w2*xd;

      return temp;
    }
    
    private:
    float * velx;
    float * vely;
    float * velz;
    long NN;
    int dim;
    double x0, x1, y0, y1, z0, z1;
    double dx, dy, dz;
    std::string fname;
  };

};

#endif
