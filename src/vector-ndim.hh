// header class for multi-dimensional vectors

// Philipp Girichidis, Oct 2010
// last change: 10 Dez 2010


#ifndef _VECTOR_NDIM_HH_
#define _VECTOR_NDIM_HH_

#include <vector>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <iomanip>

namespace vec
{

  typedef std::vector<int> int_1D;
  typedef std::vector<int_1D> int_2D;
  typedef std::vector<int_2D> int_3D;
  
  typedef std::vector<float> float_1D;
  typedef std::vector<float_1D> float_2D;
  typedef std::vector<float_2D> float_3D;
  
  typedef std::vector<double> double_1D;
  typedef std::vector<double_1D> double_2D;
  typedef std::vector<double_2D> double_3D;
  
  class double3D
  {
  public:
    double3D(unsigned int x, unsigned int y, unsigned int z)
      {
	allocate(x,y,z);
      }
    double3D(unsigned int n)
    {
      allocate(n,n,n);
    }
    void allocate(unsigned int x, unsigned int y, unsigned int z)
    {
      dimx = x;
      dimy = y;
      dimz = z;
      
      v.resize(dimx);
      for (unsigned int i = 0; i < (unsigned int)v.size(); ++i)
	{
	  v[i].resize(dimy);
	  for (unsigned int j = 0; j < (unsigned int)v[i].size(); ++j)
	    {
	      v[i][j].resize(dimz);
	      for (unsigned int k = 0; k < (unsigned int)v[i][j].size(); ++k)
		v[i][j][k] = (i+1)*100+(j+1)*10+(k+1);
	    }
	}
    }
    
    double &operator()(unsigned int x, unsigned int y, unsigned int z)
    {
      if((x<dimx) && (y<dimy) && (z<dimz))
	return v[x][y][z];
      else
	{
	  std::cerr << " wrong boundaries " << std::endl;
	  return v[0][0][0];
	}
    }
    double operator()(unsigned int x, unsigned int y, unsigned int z) const
    {
      if((x<dimx) && (y<dimy) && (z<dimz))
	return v[x][y][z];
      else
	{
	  std::cerr << " wrong boundaries " << std::endl;
	  return v[0][0][0];
	}
    }

    void print(std::ostream &out = std::cout)
    {
      std::cout << "Hallo";
      for (unsigned int i = 0; i < (unsigned int)v.size(); ++i)
	{
	  out << "x = " << i << std::endl;
	  for (unsigned int j = 0; j < (unsigned int)v[i].size(); ++j)
	    {
	      for (unsigned int k = 0; k < (unsigned int)v[i][j].size(); ++k)
		out << v[i][j][k] << " ";
	    }
	  out << std::endl;
	}
    }
  private:
    double_3D v;
    unsigned int dimx, dimy, dimz;
  };
  
}

//////////////////////////////////////////////////////
// NAMESPACE ARR
//////////////////////////////////////////////////////


namespace arr
{
  // ERROR MESSAGES

  void error_message(std::string s)
  {
    std::cerr << "ERROR: " << s << std::endl;
    std::cerr << "ABORT! " << std::endl;
    exit(1);
  }
  
  // MIN / MAX FUNCTIONS

  template <typename T>
  T max(std::vector<T> v)
  {
    T maximum  = T();
    if(v.size() > 0)
      {
	maximum = v[0];
	for(std::size_t i=1; i<v.size(); i++)
	  if(v[i] > maximum)
	    maximum = v[i];
      }
    return maximum;
  }
  
  template <typename T>
  T max(std::vector<T> v, std::size_t &imaximum)
  {
    T maximum = T();
    imaximum = 0;
    if(v.size() > 0)
      {
	maximum = v[0];
	for(std::size_t i=1; i<v.size(); i++)
	  if(v[i] > maximum)
	    {
	      imaximum = i;
	      maximum = v[i];
	    }
      }
    return maximum;
  }
  
  template <typename T>
  T max(std::vector<T> v, long int &imaximum)
  {
    return max(v,reinterpret_cast<std::size_t&>(imaximum));
  }
  
  template <typename T>
  T max(std::vector<T> v, int &imaximum)
  {
    return max(v,reinterpret_cast<std::size_t&>(imaximum));
  }
  
  template <typename T>
  T max(std::vector<T> v, unsigned int &imaximum)
  {
    return max(v,reinterpret_cast<std::size_t&>(imaximum));
  }
  
  template <typename T>
  T min(std::vector<T> v)
  {
    T minimum  = T();
    if(v.size() > 0)
      {
	minimum = v[0];
	for(std::size_t i=1; i<v.size(); i++)
	  if(v[i] < minimum)
	    minimum = v[i];
      }
    return minimum;
  }
  
  template <typename T>
  T min(std::vector<T> v, std::size_t &iminimum)
  {
    T minimum = T();
    iminimum = 0;
    if(v.size() > 0)
      {
	minimum = v[0];
	for(std::size_t i=1; i<v.size(); i++)
	  if(v[i] < minimum)
	    {
	      iminimum = i;
	      minimum = v[i];
	    }
      }
    return minimum;
  }
  
  template <typename T>
  T min(std::vector<T> v, long int &iminimum)
  {
    return min(v,reinterpret_cast<std::size_t&>(iminimum));
  }
  
  template <typename T>
  T min(std::vector<T> v, int &iminimum)
  {
    return min(v,reinterpret_cast<std::size_t&>(iminimum));
  }
  
  template <typename T>
  T min(std::vector<T> v, unsigned int &iminimum)
  {
    return min(v,reinterpret_cast<std::size_t&>(iminimum));
  }
  

  // FUNCTIONS FOR THE 2nd MIN and MAX
  template <typename T>
  T min2nd(std::vector<T> v)
  {
    T minimum1 = T();
    T minimum2 = T();
    std::size_t iminimum1 = 0;
    if(v.size() > 0)
      {
	minimum1 = v[0];
	minimum2 = v[0];
	for(std::size_t i=1; i<v.size(); i++)
	  if(v[i] < minimum1)
	    {
	      iminimum1 = i;
	      minimum1 = v[i];
	    }
	for(std::size_t i=0; i<v.size(); i++)
	  if((v[i] < minimum2) && (i != iminimum1))
	    {
	      minimum2 = v[i];
	    }
      }
    return minimum2;
  }

  template <typename T>
  T max2nd(std::vector<T> v)
  {
    T maximum1 = T();
    T maximum2 = T();
    std::size_t imaximum1 = 0;
    if(v.size() > 0)
      {
	maximum1 = v[0];
	maximum2 = v[0];
	for(std::size_t i=1; i<v.size(); i++)
	  if(v[i] > maximum1)
	    {
	      imaximum1 = i;
	      maximum1 = v[i];
	    }
	for(std::size_t i=0; i<v.size(); i++)
	  if((v[i] > maximum2) && (i != imaximum1))
	    {
	      maximum2 = v[i];
	    }
      }
    return maximum2;
  }

  // INITIALISATION

  template <typename T>
  void indgen0(std::vector<T> &v, std::size_t s=0)
  {
    if(s > 0) v.resize(s);
    for(std::size_t i=0; i<v.size(); i++)
      v[i] = (T)i;
  }
  
  template <typename T>
  void indgen(std::vector<T> &v, std::size_t s=0)
  {
    if(s > 0) v.resize(s);
    for(std::size_t i=0; i<v.size(); i++)
      v[i] = (T)(i+1);
  }
  
  template <typename T>
  void index_lin(std::vector<T> &v, T start, T end)
  {
    for(std::size_t i = 0; i<v.size(); i++)
      v[i] = start + (T)i*(end-start)/(T)(v.size()-1);
  }

  // STATISTICS
  
  template <typename T>
  T mean(std::vector<T> v)
  {
    T sum = T();
    for(std::size_t i=0; i<v.size(); i++)
      sum += v[i];
    return sum/(T)v.size();
  }

  template <typename T>
  T total(std::vector<T> v)
  {
    T sum = T();
    for(std::size_t i=0; i<v.size(); i++)
      sum += v[i];
    return sum;
  }

  template <typename T>
  T total2(std::vector<T> v)
  {
    T sum = T();
    for(std::size_t i=0; i<v.size(); i++)
      sum += v[i]*v[i];
    return sum;
  }

  template <typename T>
  T max_range3D(std::vector<T> v,
		std::size_t Nx, std::size_t Ny, std::size_t Nz,
		std::size_t ix0, std::size_t ix1,
		std::size_t iy0, std::size_t iy1,
		std::size_t iz0, std::size_t iz1)
  {
    T maximum  = T();
    if(v.size() < 1) maximum = T();
    else
      {
	// check ranges, negative values means entire range
	if(ix0 < 0)
	  {
	    ix0 = 0;
	    ix1 = Nx;
	  }
	if(iy0 < 0)
	  {
	    iy0 = 0;
	    iy1 = Ny;
	  }
	if(iz0 < 0)
	  {
	    iz0 = 0;
	    iz1 = Nz;
	  }
	maximum = v[iz0*Nx*Ny + iy0*Nx + ix0];
	for(std::size_t iz=iz0; iz<iz1; iz++)
	  for(std::size_t iy=iy0; iy<iy1; iy++)
	    for(std::size_t ix=ix0; ix<ix1; ix++)
	      if(v[iz*Nx*Ny + iy*Nx + ix] > maximum)
		maximum = v[iz*Nx*Ny + iy*Nx + ix];
      }
    return maximum;
  }
  
  template <typename T>
  T min_range3D(std::vector<T> v,
		std::size_t Nx, std::size_t Ny, std::size_t Nz,
		std::size_t ix0, std::size_t ix1,
		std::size_t iy0, std::size_t iy1,
		std::size_t iz0, std::size_t iz1)
  {
    T minimum  = T();
    if(v.size() < 1) minimum = T();
    else
      {
	// check ranges, negative values means entire range
	if(ix0 < 0)
	  {
	    ix0 = 0;
	    ix1 = Nx;
	  }
	if(iy0 < 0)
	  {
	    iy0 = 0;
	    iy1 = Ny;
	  }
	if(iz0 < 0)
	  {
	    iz0 = 0;
	    iz1 = Nz;
	  }
	minimum = v[iz0*Nx*Ny + iy0*Nx + ix0];
	for(std::size_t iz=iz0; iz<iz1; iz++)
	  for(std::size_t iy=iy0; iy<iy1; iy++)
	    for(std::size_t ix=ix0; ix<ix1; ix++)
	      if(v[iz*Nx*Ny + iy*Nx + ix] < minimum)
		minimum = v[iz*Nx*Ny + iy*Nx + ix];
      }
    return minimum;
  }

  template <typename T>
  void print(std::vector<T> &v, std::ostream &out = std::cout)
  {
    for(std::size_t i=0; i<v.size(); i++)
      out << v[i] << " ";
    out << std::endl;
  }

  // MATHEMATICAL FUNCTIONS
  template <typename T>
  void add(std::vector<T> &s, std::vector<T> a, std::vector<T> b)
  {
    if(a.size() != b.size())
      error_message("arr::add : a and b have different sizes");
    s.resize(a.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = a[i] + b[i];
  }
  template <typename T>
  void add(std::vector<T> &s, T a)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] += a;
  }
  template <typename T>
  void sub(std::vector<T> &s, std::vector<T> a, std::vector<T> b)
  {
    if(a.size() != b.size())
      error_message("arr::sub : a and b have different sizes");
    s.resize(a.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = a[i] - b[i];
  }
  template <typename T>
  void sub(std::vector<T> &s, T a)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] -= a;
  }

  template <typename T>
  void mult(std::vector<T> &s, std::vector<T> a, std::vector<T> b)
  {
    if(a.size() != b.size())
      error_message("arr::mult : a and b have different sizes");
    s.resize(a.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = a[i] * b[i];
  }
  template <typename T>
  void mult(std::vector<T> &s, std::vector<T> a, T f)
  {
    s.resize(a.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = f * a[i];
  }


  // WHERE functions
  template <typename T>
  std::vector<bool> where_lt(std::vector<T> v, T x)
  {
    std::vector<bool> tmp(v.size(),false);
    for(std::size_t i=0; i<v.size(); i++)
      if(v[i] < x) tmp[i] = true;
    return tmp;
  }
  template <typename T>
  std::vector<bool> where_le(std::vector<T> v, T x)
  {
    std::vector<bool> tmp(v.size(),false);
    for(std::size_t i=0; i<v.size(); i++)
      if(v[i] <= x) tmp[i] = true;
    return tmp;
  }
  template <typename T>
  std::vector<bool> where_gt(std::vector<T> v, T x)
  {
    std::vector<bool> tmp(v.size(),false);
    for(std::size_t i=0; i<v.size(); i++)
      if(v[i] > x) tmp[i] = true;
    return tmp;
  }
  template <typename T>
  std::vector<bool> where_ge(std::vector<T> v, T x)
  {
    std::vector<bool> tmp(v.size(),false);
    for(std::size_t i=0; i<v.size(); i++)
      if(v[i] >= x) tmp[i] = true;
    return tmp;
  }
  template <typename T>
  std::vector<bool> where_eq(std::vector<T> v, T x)
  {
    std::vector<bool> tmp(v.size(),false);
    for(std::size_t i=0; i<v.size(); i++)
      if(v[i] == x) tmp[i] = true;
    return tmp;
  }

  template <typename T>
  void rescale(std::vector<T> &v, T min, T max)
  {
    T vec_min;
    T vec_max;
    if(v.size()>0)
      {
	vec_min = v[0];
	vec_max = v[0];
      }
    // find min/max
    for(std::size_t i = 1; i<v.size(); i++)
      {
	if(v[i] > vec_max)
	  vec_max = v[i];
	if(v[i] < vec_min)
	  vec_min = v[i];
      }
    // rescale
    for(std::size_t i = 1; i<v.size(); i++)
      v[i] = min + (v[i]-vec_min)/(vec_max-vec_min)*(max-min);
  }

  template <typename T>
  bool diff(std::vector<T> a, std::vector<T> b)
  {
    bool differ = false;
    if(a.size() != b.size())
      {
	differ = true;
	return differ;
      }
    for(std::size_t i=0; i<a.size(); i++)
      if(a[i] != b[i]) differ = true;
    return differ;
  }


  // SPECIALISED TEMPLATE FUNCTIONS
  void norm(std::vector<double> x, std::vector<double> y, std::vector<double> z, std::vector<double> &s)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = sqrt(x[i]*x[i] + y[i]*y[i] + z[i]*z[i]);
  }
  std::vector<double> norm(std::vector<double> x, std::vector<double> y, std::vector<double> z)
  {
    std::vector<double> s(x.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = sqrt(x[i]*x[i] + y[i]*y[i] + z[i]*z[i]);
    return s;
  }
  void norm2(std::vector<double> x, std::vector<double> y, std::vector<double> z, std::vector<double> &s)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = x[i]*x[i] + y[i]*y[i] + z[i]*z[i];
  }
  std::vector<double> norm2(std::vector<double> x, std::vector<double> y, std::vector<double> z)
  {
    std::vector<double> s(x.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = x[i]*x[i] + y[i]*y[i] + z[i]*z[i];
    return s;
  }
  void abs(std::vector<double> &s)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = fabs(s[i]);
  }
  void square(std::vector<double> &s)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = s[i]*s[i];
  }
  std::vector<double> sqr(std::vector<double> a)
  {
    std::vector<double> s(a.size());
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = a[i]*a[i];
    return s;
  }
  void sqrt(std::vector<double> &s)
  {
    for(std::size_t i = 0; i<s.size(); i++)
      s[i] = std::sqrt(s[i]);
  }
  double abs_total(std::vector<double> s)
  {
    double sum = 0.0;
    for(std::size_t i = 0; i<s.size(); i++)
      sum += fabs(s[i]);
    return sum;
  }
  double stddev(std::vector<double> v, double &mean_)
  {
    // return also mean, because it is for free
    mean_ = mean(v);
    double stddev = 0.0;
    for(std::size_t i = 0; i<v.size(); i++)
      stddev += pow(v[i]-mean_, 2.0);
    return std::sqrt(stddev/(v.size()-1));
  }
  double stddev(std::vector<double> v)
  {
    double dummy;
    return stddev(v,dummy);
  }
  void statist_moments(std::vector<double> v, double &mean, double &stddev, double &skew)
  {
    // mean
    mean = 0.0;
    for(std::size_t i=0; i<v.size(); i++)
      mean += v[i];
    mean = mean/(double)v.size();

    // standard deviation and skewness
    stddev = 0.0;
    skew   = 0.0;
    for(std::size_t i = 0; i<v.size(); i++)
      {
	stddev += pow(v[i]-mean, 2.0);
	skew   += pow(v[i]-mean, 3.0);
      }
    stddev = std::sqrt(stddev/(v.size()-1));
    skew   = skew/(double)v.size()/pow(stddev,3.0);
  }
  

  //////////////////////////////////////////////////
  //
  //  1D vector
  //
  //////////////////////////////////////////////////

  template <class T>
  class v1D : public std::vector<T>
  {
  public:
    v1D()
    {
    }
    v1D(size_t NN)
    {
      (*this).resize(NN);
    }
    v1D(size_t NN, T val)
    {
      (*this).assign(NN,val);
    }
    ~v1D()
    {
    }
    //v1D(size_t size) : std::vector(size) {}
    //v1D(size_t size, T val) : std::vector(val,size) {}

    // access operators
    T& operator()(std::size_t i)
    {
      return (*this)[i];
    }
    T operator()(std::size_t i) const
    {
      return (*this)[i];
    }
   
    // modify vectors
    void add(std::vector<T> a)
    {
      a.resize((*this).size());
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] += a[i];
    }
    void add(T a)
    {
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] += a;
    }

    void sub(std::vector<T> a)
    {
      a.resize((*this).size());
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] -= a[i];
    }
    void sub(T a)
    {
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] -= a;
    }

    void mult(std::vector<T> a)
    {
      a.resize((*this).size());
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] *= a[i];
    }
    void mult_sqr(std::vector<T> a)
    {
      a.resize((*this).size());
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] *= a[i]*a[i];
    }
    void mult(T f)
    {
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] *= f;
    }
    void sqr()
    {
      for(std::size_t i = 0; i<(*this).size(); i++)
	(*this)[i] *= (*this)[i];
    }

    // statistics
    T mean()
    {
      T sum = T();
      for(std::size_t i = 0; i<(*this).size(); i++)
	sum += (*this)[i];
      return sum/(T)(*this).size();
    }
    T median()
    {
      if((*this).size()%2 == 1) return (*this)[(*this).size()%2];
      else return 0.5*((*this)[(*this).size()%2-1]+(*this)[(*this).size()%2]);
    }

    T total()
    {
      T sum = T();
      for(std::size_t i = 0; i<(*this).size(); i++)
	sum += (*this)[i];
      return sum;
    }

    T max()
    {
      T maximum  = T();
      if((*this).size() < 1) maximum = T();
      else
	{
	  maximum = (*this)[0];
	  for(std::size_t i=1; i<(*this).size(); i++)
	    if((*this)[i] > maximum)
	      maximum = (*this)[i];
	}
      return maximum;
    }
    T min()
    {
      T minimum  = T();
      if((*this).size() < 1) minimum = T();
      else
	{
	  minimum = (*this)[0];
	  for(std::size_t i=1; i<(*this).size(); i++)
	    if((*this)[i] < minimum)
	      minimum = (*this)[i];
	}
      return minimum;
    }
    void minmax(T &min, T &max)
    {
      if((*this).size() < 1)
	{
	  max = T();
	  min = T();
	}
      else
	{
	  max = (*this)[0];
	  min = (*this)[0];
	  for(std::size_t i=1; i<(*this).size(); i++)
	    {
	      if((*this)[i] > max)
		max = (*this)[i];
	      if((*this)[i] < min)
		min = (*this)[i];
	    }
	}
    }

    void indgen()
    {
      for(std::size_t i=0; i<(*this).size(); i++)
	(*this)[i] = (T)(i+1);
    }
    
    void indgen0()
    {
      for(std::size_t i=0; i<(*this).size(); i++)
	(*this)[i] = (T)(i);
    }
    
    // WHERE functions
    std::vector<bool> where_lt(T x)
    {
      std::vector<bool> tmp((*this).size(),false);
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] < x) tmp[i] = true;
      return tmp;
    }
    std::vector<bool> where_le(T x)
    {
      std::vector<bool> tmp((*this).size(),false);
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] <= x) tmp[i] = true;
      return tmp;
    }
    std::vector<bool> where_gt(T x)
    {
      std::vector<bool> tmp((*this).size(),false);
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] > x) tmp[i] = true;
      return tmp;
    }
    std::vector<bool> where_ge(T x)
    {
      std::vector<bool> tmp((*this).size(),false);
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] >= x) tmp[i] = true;
      return tmp;
    }
    std::vector<bool> where_eq(T x)
    {
      std::vector<bool> tmp((*this).size(),false);
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] == x) tmp[i] = true;
      return tmp;
    }

    void where_lt(std::vector<bool> b, T x)
    {
      b.resize((*this).size());
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] < x) b[i] = true;
	else b[i] = false;
    }
    void where_le(std::vector<bool> b, T x)
    {
      b.resize((*this).size());
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] <= x) b[i] = true;
	else b[i] = false;
    }
    void where_gt(std::vector<bool> b, T x)
    {
      b.resize((*this).size());
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] > x) b[i] = true;
	else b[i] = false;
    }
    void where_ge(std::vector<bool> b, T x)
    {
      b.resize((*this).size());
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] >= x) b[i] = true;
	else b[i] = false;
    }
    void where_eq(std::vector<bool> b, T x)
    {
      b.resize((*this).size());
      for(std::size_t i=0; i<(*this).size(); i++)
	if((*this)[i] == x) b[i] = true;
	else b[i] = false;
    }

    void rescale(T min, T max)
    {
      T vec_min;
      T vec_max;
      if((*this).size()>0)
	{
	  vec_min = (*this)[0];
	  vec_max = (*this)[0];
	}
      // find min/max
      for(std::size_t i = 1; i<(*this).size(); i++)
	{
	  if((*this)[i] > vec_max)
	    vec_max = (*this)[i];
	  if((*this)[i] < vec_min)
	    vec_min = (*this)[i];
	}
      // rescale
      for(std::size_t i = 1; i<(*this).size(); i++)
	(*this)[i] = min + ((*this)[i]-vec_min)/(vec_max-vec_min)*(max-min);
    }
    
    // print
    void print(std::ostream & out = std::cout)
    {
      for(size_t i=0; i<(*this).size(); i++)
	out << " " << (*this)[i];
      out << std::endl;
    }
    void print_dat_form(std::ostream &out = std::cout)
    {
      for (std::size_t i = 0; i < (*this).size(); i++)
	out << std::setw(14) << i
	    << std::setw(14) << (*this)[i] << std::endl;
    }

    // convert to ordinary c array
    void c_arr(T c[], std::size_t N)
    {
      for(std::size_t i = 0; i<(*this).size(); i++)
	c[i] = (*this)[i];
      for(std::size_t i = (*this).size(); i<N; i++)
	c[i] = T();
    }

    void merge(std::vector<T> v, std::size_t pos=0)
    {
      for(std::size_t i = 0; i<v.size(); i++)
	(*this)[(i+pos)%(*this).size()] = v[i];
    }
    
    void mirror(double pos, bool low_to_high=true)
    {
      std::size_t i0 = 0;
      std::size_t i1 = (std::size_t)pos;
      if(!low_to_high)
	{
	  i0 = i1+1;
	  i1 = (*this).size();
	}
      for(std::size_t i = i0; i<i1; i++)
	(*this)[(((long)round(2.0*pos))-i+(*this).size())%(*this).size()]
	  = (*this)[i%(*this).size()];
    }

    void xreverse(std::size_t i0, std::size_t i1)
    {
      for(std::size_t i=i0; i<=i1; i++)
	{
	  T tmp = (*this)[i];
	  (*this)[i] = (*this)[i1+i0-i];
	  (*this)[i1+i0-i] = tmp;
	}
    }
    void reverse(std::size_t i0=0, std::size_t i1=0)
    {
      if(i1 == 0) i1 = (*this).size()-1;
      for(std::size_t i=i0; i<=(i0+i1)/2; i++)
	{
	  T tmp = (*this)[i];
	  (*this)[i] = (*this)[i1+i0-i];
	  (*this)[i1+i0-i] = tmp;
	}
    }


  };

  //////////////////////////////////////////////////
  //
  //  2D vector
  //
  //////////////////////////////////////////////////


  template <class T>
  class v2D : public arr::v1D<T>
  {
  public:
    v2D()
    {
      dimx = 0;
      dimy = 0;
    }
    v2D(std::size_t x, std::size_t y, T init=T())
    {
      (*this).assign(x*y,init);
      dimx = x;
      dimy = y;
    }
    v2D(std::size_t n, T init=T())
    {
      (*this).assign(n*n,init);
      dimx = n;
      dimy = n;
    }

    ~v2D()
    {
    }
    
    T& operator()(std::size_t x, std::size_t y)
    {
      return (*this)[dimx*y + x];
    }
    T operator()(std::size_t x, std::size_t y) const
    {
      return (*this)[dimx*y + x];
    }

    T& operator()(std::size_t i)
    {
      return (*this)[i];
    }
    T operator()(std::size_t i) const
    {
      return (*this)[i];
    }

    void resize2D(std::size_t xx, std::size_t yy)
    {
      (*this).resize(xx*yy);
      dimx = xx;
      dimy = yy;
    }

    void print_dat_form(std::ostream &out = std::cout)
    {
      for (std::size_t iy = 0; iy < dimy; iy++)
	{
	  for (std::size_t ix = 0; ix < dimx; ix++)
	    out << std::setw(14) << ix 
		<< std::setw(14) << iy
		<< std::setw(14) << (*this)[dimx*iy + ix] << std::endl;
	  out << std::endl;
	}
    }

  private:
    std::size_t dimx, dimy;
  };
  
  //////////////////////////////////////////////////
  //
  //  3D vector
  //
  //////////////////////////////////////////////////


  template <class T>
  class v3D : public arr::v1D<T> //std::vector<T>
  {
  public:
    v3D()
    {
      dimx = 0;
      dimy = 0;
      dimz = 0;
    }
    v3D(std::size_t x, std::size_t y, std::size_t z, T init=T())
    {
      (*this).assign(x,y,z,init);
    }
    v3D(std::size_t n, T init=T())
    {
      (*this).assign(n,init);
    }

    void assign(std::size_t x, std::size_t y, std::size_t z, T init=T())
    {
      // call assign from base class
      (*this).std::vector<T>::assign(x*y*z,init);
      dimx = x;
      dimy = y;
      dimz = z;
    }
    void assign(std::size_t n, T init=T())
    {
      // call assign from base class
      (*this).std::vector<T>::assign(n*n*n,init);
      dimx = n;
      dimy = n;
      dimz = n;
    }

    void resize(std::size_t x, std::size_t y, std::size_t z)
    {
      // call resize from base class
      (*this).std::vector<T>::resize(x*y*z);
      dimx = x;
      dimy = y;
      dimz = z;
    }
    void resize(std::size_t n)
    {
      // call resize from base class
      (*this).std::vector<T>::resize(n*n*n);
      dimx = n;
      dimy = n;
      dimz = n;
    }

    void assign3D(std::size_t x, std::size_t y, std::size_t z, T init=T())
    {
      (*this).assign(x,y,z,init);
    }

    void assign3D(std::size_t n, T init=T())
    {
      (*this).assign(n,init);
    }

//     T length(std::size_t ix, std::size_t iy, std::size_t iz)
//     {
//       return sqrt(pow((double)v[dimx*dimy*iz + dimx*iy + ix],2) 
// 		  + pow((double)v[dimx*dimy*iz + dimx*iy + ix],2)
// 		  + pow((double)v[dimx*dimy*iz + dimx*iy + ix],2));
//     }
    
//     void allocate(std::size_t x, std::size_t y, std::size_t z)
//     {
//       dimx = x;
//       dimy = y;
//       dimz = z;
//       v.assign(x*y*z,0.0);
//     }
    ~v3D()
    {
    }
    
//     T& operator()(std::size_t x, std::size_t y, std::size_t z)
//     {
//       return (*this)[dimx*dimy*((long)(dimz+z)%(long)dimz) + dimx*((long)(dimy+y)%(long)dimy) + (long)(dimx+x)%(long)dimx];
//     }
//     T operator()(std::size_t x, std::size_t y, std::size_t z) const
//     {
//       return (*this)[dimx*dimy*((long)(dimz+z)%(long)dimz) + dimx*((long)(dimy+y)%(long)dimy) + (long)(dimx+x)%(long)dimx];
//     }

//     T& operator()(std::size_t x, std::size_t y, std::size_t z)
//     {
//       return (*this)[dimx*dimy*z + dimx*y + x];
//     }
//     T operator()(std::size_t x, std::size_t y, std::size_t z) const
//     {
//       return (*this)[dimx*dimy*z + dimx*y + x];
//     }

    // the "long" version causes problems if called with actual numbers instead of variables
    // but need "long" because of negative arguments
    T& operator()(long x, long y, long z)
    {
      //while(x<0) x += dimx;
      //while(y<0) y += dimy;
      //while(z<0) z += dimz;
      return (*this)[dimx*dimy*((dimz+z)%dimz) + dimx*((dimy+y)%dimy) + (dimx+x)%dimx];
    }
    T operator()(long x, long y, long z) const
    {
      //while(x<0) x += dimx;
      //while(y<0) y += dimy;
      //while(z<0) z += dimz;
      return (*this)[dimx*dimy*((dimz+z)%dimz) + dimx*((dimy+y)%dimy) + (dimx+x)%dimx];
    }


    T& operator()(std::size_t i)
    {
      return (*this)[i];
    }
    T operator()(std::size_t i) const
    {
      return (*this)[i];
    }

    std::size_t sizex()
    {
      return dimx;
    }
    std::size_t sizey()
    {
      return dimy;
    }
    std::size_t sizez()
    {
      return dimz;
    }

    void set(T val)
    {
      for(std::size_t i=0; i<(*this).size(); i++)
	(*this)[i] = 1.0;
    }
    
    void resize3D(std::size_t x, std::size_t y, std::size_t z)
    {
      (*this).resize(x*y*z);
      dimx = x;
      dimy = y;
      dimz = z;
    }
    void set_lin(std::size_t i, T val)
    {
      if(i < dimx*dimy*dimz)
	(*this)[i] = val;
      else
	std::cout << "wrong array range!" << std::endl;
    }
    T get_lin(std::size_t i)
    {
      if(i < dimx*dimy*dimz)
	return (*this)[i];
      else
	{
	  std::cout << "wrong array range!" << std::endl;
	  return 0.0;
	}
    }

    void mult3D(std::size_t x0, std::size_t x1,
		std::size_t y0, std::size_t y1,
		std::size_t z0, std::size_t z1, T factor)
    {
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    (*this)[iz*dimx*dimy + iy*dimx + ix] *= factor;
    }

    void indgen3D(std::size_t x0=0, std::size_t x1=0,
		  std::size_t y0=0, std::size_t y1=0,
		  std::size_t z0=0, std::size_t z1=0)
    {
      // that is a weird function, just assignes ix+iy+iz to v[ix][iy][iz]
      if(x1 == 0) x1 = dimx-1;
      if(y1 == 0) y1 = dimy-1;
      if(z1 == 0) z1 = dimz-1;
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    (*this)[iz*dimx*dimy + iy*dimx + ix] = ix+iy+iz;
    }

    void clear(std::size_t x0=0, std::size_t x1=0,
	       std::size_t y0=0, std::size_t y1=0,
	       std::size_t z0=0, std::size_t z1=0)
    {   
      if(x1 == 0) x1 = dimx-1;
      if(y1 == 0) y1 = dimy-1;
      if(z1 == 0) z1 = dimz-1;
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    (*this)[iz*dimx*dimy + iy*dimx + ix] = T();
    }

    void reverse3D(std::size_t x0=0, std::size_t x1=0,
		   std::size_t y0=0, std::size_t y1=0,
		   std::size_t z0=0, std::size_t z1=0)
    {   
      //if(x1 == 0) x1 = dimx-1;
      //if(y1 == 0) y1 = dimy-1;
      //if(z1 == 0) z1 = dimz-1;
      //std::cout << "bnds: " << x1 << " " << y1 << " " << z1 << std::endl;
      for(std::size_t iz=z0; iz<z0+(z1-z0+1)/2; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    {
	      //std::cout << ix << " " << iy << " swap " 
	      //<< (*this)[iz*dimx*dimy + iy*dimx + ix] << "  "
	      //<<(*this)[(z1+z0-iz)*dimx*dimy + (y1+y0-iy)*dimx + x1+x0-ix] << std::endl;
	      T tmp = (*this)[iz*dimx*dimy + iy*dimx + ix];
	      (*this)[iz*dimx*dimy + iy*dimx + ix] =
		(*this)[(z1+z0-iz)*dimx*dimy + (y1+y0-iy)*dimx + x1+x0-ix];
	      (*this)[(z1+z0-iz)*dimx*dimy + (y1+y0-iy)*dimx + x1+x0-ix] = tmp;
	    }
      // if #z is odd, swap middle plane separately
      if(true)
      if((z1-z0)%2 == 0)
	{
	  std::size_t iz = (z0+z1)/2;
	  //std::cout << "sss" << iz << std::endl;
	  for(std::size_t iy=y0; iy<=(y1+y0)/2; iy++)
	    for(std::size_t ix=x0; ix<=x1; ix++)
	      {
		// do not swap entire middle y line if #y is odd
		if( !(((y1-y0)%2 == 0) && (iy == (y1+y0)/2) && (ix > (x1+x0)/2)))
		  {
		    //std::cout << ix << " " << iy << " swap " 
		    //<< (*this)[iz*dimx*dimy + iy*dimx + ix] << "  "
		    //<<(*this)[(z1+z0-iz)*dimx*dimy + (y1+y0-iy)*dimx + x1+x0-ix] << std::endl;
		    T tmp = (*this)[iz*dimx*dimy + iy*dimx + ix];
		    (*this)[iz*dimx*dimy + iy*dimx + ix] =
		      (*this)[(z1+z0-iz)*dimx*dimy + (y1+y0-iy)*dimx + x1+x0-ix];
		    (*this)[(z1+z0-iz)*dimx*dimy + (y1+y0-iy)*dimx + x1+x0-ix] = tmp;
		  }
	      }
	} 
    }

    void shift2(std::size_t x0, std::size_t x1,
		std::size_t y0, std::size_t y1,
		std::size_t z0, std::size_t z1,
		long int ddx, long int ddy, long int ddz, bool copy=true)
    {
      std::size_t dx = ((ddx%dimx)+dimx)%dimx;
      std::size_t dy = ((ddy%dimy)+dimy)%dimy;
      std::size_t dz = ((ddz%dimz)+dimz)%dimz;
      std::cout << "shifts:" << dx << " " << dy << " " << dz << std::endl;
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    {
	      (*this)((ix+dx)%dimx, (iy+dy)%dimy, (iz+dz)%dimz) = (*this)(ix,iy,iz);
	      if(!copy)
		(*this)(ix,iy,iz) = T();
	    }
    }

    void shift(std::size_t x0, std::size_t x1,
	       std::size_t y0, std::size_t y1,
	       std::size_t z0, std::size_t z1,
	       long int ddx, long int ddy, long int ddz, bool copy=true)
    {
      v3D<T> tmp(dimx,dimy,dimz);
      std::size_t dx = ((ddx%dimx)+dimx)%dimx;
      std::size_t dy = ((ddy%dimy)+dimy)%dimy;
      std::size_t dz = ((ddz%dimz)+dimz)%dimz;
      //std::cout << "shifts:" << dx << " " << dy << " " << dz << std::endl;
      // shift to tmp array
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    {
	      tmp((ix+dx)%dimx, (iy+dy)%dimy, (iz+dz)%dimz) = (*this)(ix,iy,iz);
	    }
      // copy array back
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    (*this)((ix+dx)%dimx, (iy+dy)%dimy, (iz+dz)%dimz)
	      = tmp((ix+dx)%dimx, (iy+dy)%dimy, (iz+dz)%dimz);
    }

    void merge3D(std::vector<double> v,
		 std::size_t dx, std::size_t dy, std::size_t dz,
		 std::size_t x,  std::size_t y,  std::size_t z)
    {
      // merges general linear vector v into (*this) at position x,y,z
      // with sizes dx, dy, dz
      if(v.size() != dx*dy*dz)
	std::cerr << "arr::v3D::merge() given vector does not have the correct size" << std::endl;
      for(std::size_t iz=0; iz<dz; iz++)
	for(std::size_t iy=0; iy<dy; iy++)
	  for(std::size_t ix=0; ix<dx; ix++)
	    (*this)((x+ix)%dimx, (y+iy)%dimy, (z+iz)%dimz)
	      = v[iz*dy*dx + iy*dx + ix];
    }

    void shift(long int ddx, long int ddy, long int ddz, bool copy=true)
    {
      shift(0,dimx-1,0,dimy-1,0,dimz-1,ddx,ddy,ddz,copy);
    }

    void mirrorX(double xm, bool low_to_high=true)
    {
      std::size_t x0 = 0;
      std::size_t x1 = (std::size_t)xm;
      if(!low_to_high)
	{
	  x0 = x1+1;
	  x1 = dimx-1;
	}
      for(std::size_t iz=0; iz<dimz; iz++)
	for(std::size_t iy=0; iy<dimy; iy++)
	  for(std::size_t ix=x0; ix<=x1; ix++)
	    {
	      (*this)[iz*dimx*dimy + iy*dimx + (((long)round(2.0*xm))-ix+dimx)%dimx]
		= (*this)[iz*dimx*dimy + iy*dimx + ix%dimx];
	    }
    }
    void mirrorX(std::size_t xm, bool low_to_high=true)
    {
      mirrorX((std::size_t) xm, low_to_high);
    }

    void mirrorY(double ym, bool low_to_high=true)
    {
      std::size_t y0 = 0;
      std::size_t y1 = (std::size_t)ym;
      if(!low_to_high)
	{
	  y0 = y1+1;
	  y1 = dimy-1;
	}
      for(std::size_t iz=0; iz<dimz; iz++)
	for(std::size_t iy=y0; iy<=y1; iy++)
	  for(std::size_t ix=0; ix<dimx; ix++)
	    {
	      (*this)[iz*dimx*dimy + ((((long)round(2.0*ym))-iy+dimy)%dimy)*dimx + ix]
		= (*this)[iz*dimx*dimy + (iy%dimy)*dimx + ix];
	    }
    }
    void mirrorY(std::size_t ym, bool low_to_high=true)
    {
      mirrorY((std::size_t) ym, low_to_high);
    }

    void mirrorZ(double zm, bool low_to_high=true)
    {
      std::size_t z0 = 0;
      std::size_t z1 = (std::size_t)zm;
      if(!low_to_high)
	{
	  z0 = z1+1;
	  z1 = dimz-1;
	}
      for(std::size_t iz=z0; iz<=z1; iz++)
	for(std::size_t iy=0; iy<dimy; iy++)
	  for(std::size_t ix=0; ix<dimx; ix++)
	    {
	      (*this)[((((long)round(2.0*zm))-iz+dimz)%dimz)*dimx*dimy + iy*dimx + ix]
		= (*this)[(iz%dimz)*dimx*dimy + iy*dimx + ix];
	    }
    }
    void mirrorZ(std::size_t zm, bool low_to_high=true)
    {
      mirrorZ((std::size_t) zm, low_to_high);
    }

    // not tested
    T* c_arr()
    {
      return &(*this).front();
    }

//     std::size_t size()
//     {
//       return (*this).size();
//     }

    // statistics
    T max()
    {
      T maximum  = T();
      if((*this).size() < 1) maximum = T();
      else
	{
	  maximum = (*this)[0];
	  for(std::size_t i=1; i<(*this).size(); i++)
	    if((*this)[i] > maximum)
	      maximum = (*this)[i];
	}
      return maximum;
    }
    T min()
    {
      T minimum  = T();
      if((*this).size() < 1) minimum = T();
      else
	{
	  minimum = (*this)[0];
	  for(std::size_t i=1; i<(*this).size(); i++)
	    if((*this)[i] < minimum)
	      minimum = (*this)[i];
	}
      return minimum;
    }

//     void minmax(T &min, T &max)
//     {
//       if((*this).size() < 1)
// 	{
// 	  max = T();
// 	  min = T();
// 	}
//       else
// 	{
// 	  max = (*this)[0];
// 	  min = (*this)[0];
// 	  for(std::size_t i=1; i<(*this).size(); i++)
// 	    {
// 	      if((*this)[i] > max)
// 		max = (*this)[i];
// 	      if((*this)[i] < min)
// 		min = (*this)[i];
// 	    }
// 	}
//     }

    T max_range(std::size_t ix0, std::size_t ix1,
		std::size_t iy0, std::size_t iy1,
		std::size_t iz0, std::size_t iz1)
    {
      T maximum  = T();
      if((*this).size() < 1) maximum = T();
      else
	{
	  // check ranges, negative values means entire range
	  if(ix0 < 0)
	    {
	      ix0 = 0;
	      ix1 = dimx;
	    }
	  if(iy0 < 0)
	    {
	      iy0 = 0;
	      iy1 = dimy;
	    }
	  if(iz0 < 0)
	    {
	      iz0 = 0;
	      iz1 = dimz;
	    }
	  maximum = (*this)[iz0*dimx*dimy + iy0*dimx + ix0];
	  for(std::size_t iz=iz0; iz<iz1; iz++)
	    for(std::size_t iy=iy0; iy<iy1; iy++)
	      for(std::size_t ix=ix0; ix<ix1; ix++)
		if((*this)[iz*dimx*dimy + iy*dimx + ix] > maximum)
		  maximum = (*this)[iz*dimx*dimy + iy*dimx + ix];
	}
      return maximum;
    }

    T min_range(std::size_t ix0, std::size_t ix1,
		std::size_t iy0, std::size_t iy1,
		std::size_t iz0, std::size_t iz1)
    {
      T minimum  = T();
      if((*this).size() < 1) minimum = T();
      else
	{
	  // check ranges, negative values means entire range
	  if(ix0 < 0)
	    {
	      ix0 = 0;
	      ix1 = dimx;
	    }
	  if(iy0 < 0)
	    {
	      iy0 = 0;
	      iy1 = dimy;
	    }
	  if(iz0 < 0)
	    {
	      iz0 = 0;
	      iz1 = dimz;
	    }
	  minimum = (*this)[iz0*dimx*dimy + iy0*dimx + ix0];
	  for(std::size_t iz=iz0; iz<iz1; iz++)
	    for(std::size_t iy=iy0; iy<iy1; iy++)
	      for(std::size_t ix=ix0; ix<ix1; ix++)
		if((*this)[iz*dimx*dimy + iy*dimx + ix] < minimum)
		  minimum = (*this)[iz*dimx*dimy + iy*dimx + ix];
	}
      return minimum;
    }

    void print3D(std::ostream &out = std::cout)
    {
      for (std::size_t iz = 0; iz < dimz; iz++)
	{
	  for (std::size_t iy = 0; iy < dimy; iy++)
	    {
	      for (std::size_t ix = 0; ix < dimx; ix++)
		out << (*this)[dimx*dimy*iz + dimx*iy + ix] << " ";
	      out << std::endl;
	    }
	  out << std::endl;
	}
    }

    void print2(std::ostream &out = std::cout)
    {
      for (std::size_t iz = 0; iz < dimz; iz++)
	{
	  for (std::size_t iy = 0; iy < dimy; iy++)
	    {
	      for (std::size_t ix = 0; ix < dimx; ix++)
		out << (*this)[dimx*dimy*iz + dimx*iy + ix] << " ";
	      out << "   ";
	    }
	  out << std::endl;
	}
    }

    void print_indx(std::ostream &out = std::cout)
    {
      out << "print index" << std::endl;
      for (std::size_t iz = 0; iz < dimz; iz++)
	{
	  for (std::size_t iy = 0; iy < dimy; iy++)
	    {
	      for (std::size_t ix = 0; ix < dimx; ix++)
		out << "  x=" << ix 
		    << "  y=" << iy
		    << "  z=" << iz 
		    << "  v[]=" << (*this)[dimx*dimy*iz + dimx*iy + ix] << std::endl;
	    }
	  out << std::endl;
	}
    }

    void print_dat_form(std::ostream &out = std::cout)
    {
      for (std::size_t iz = 0; iz < dimz; iz++)
	{
	  for (std::size_t iy = 0; iy < dimy; iy++)
	    {
	      for (std::size_t ix = 0; ix < dimx; ix++)
		out << std::setw(14) << ix 
		    << std::setw(14) << iy
		    << std::setw(14) << iz 
		    << std::setw(14) << (*this)[dimx*dimy*iz + dimx*iy + ix] << std::endl;
	      out << std::endl;
	    }
	  out << std::endl;
	}
    }


    void print_sheet(std::size_t iz, std::ostream &out = std::cout)
    {
      for (std::size_t iy = 0; iy < dimy; iy++)
	{
	  for (std::size_t ix = 0; ix < dimx; ix++)
	    out << (*this)[dimx*dimy*iz + dimx*iy + ix] << " ";
	  out << std::endl;
	}
    }
    void print_sheetX(std::size_t ix, std::ostream &out = std::cout)
    {
      for (std::size_t iz = 0; iz < dimz; iz++)
	{
	  for (std::size_t iy = 0; iy < dimy; iy++)
	    out << (*this)[dimx*dimy*iz + dimx*iy + ix] << " ";
	  out << std::endl;
	}
    }
    void print_sheetY(std::size_t iy, std::ostream &out = std::cout)
    {
      for (std::size_t iz = 0; iz < dimz; iz++)
	{
	  for (std::size_t ix = 0; ix < dimx; ix++)
	    out << (*this)[dimx*dimy*iz + dimx*iy + ix] << " ";
	  out << std::endl;
	}
    }
    void print_sheetZ(std::size_t iz, std::ostream &out = std::cout)
    {
      for (std::size_t iy = 0; iy < dimy; iy++)
	{
	  for (std::size_t ix = 0; ix < dimx; ix++)
	    out << (*this)[dimx*dimy*iz + dimx*iy + ix] << " ";
	  out << std::endl;
	}
    }


    void get_sheet(std::size_t iz, std::vector<T> &data)
    {
      data.resize(dimx*dimy);
      for (std::size_t iy = 0; iy < dimy; iy++)
	for (std::size_t ix = 0; ix < dimx; ix++)
	  data[iy*dimx+ix] = (*this)[dimx*dimy*iz + dimx*iy + ix];
    }

    void get_sheetX(std::size_t ix, std::vector<T> &data)
    {
      data.resize(dimy*dimz);
      for (std::size_t iz = 0; iz < dimz; iz++)
	for (std::size_t iy = 0; iy < dimy; iy++)
	  data[iz*dimy+iy] = (*this)[dimx*dimy*iz + dimx*iy + ix];
    }
    void get_sheetY(std::size_t iy, std::vector<T> &data)
    {
      data.resize(dimx*dimz);
      for (std::size_t iz = 0; iz < dimz; iz++)
	for (std::size_t ix = 0; ix < dimx; ix++)
	  data[iz*dimx+ix] = (*this)[dimx*dimy*iz + dimx*iy + ix];
    }
    void get_sheetZ(std::size_t iz, std::vector<T> &data)
    {
      data.resize(dimx*dimy);
      for (std::size_t iy = 0; iy < dimy; iy++)
	for (std::size_t ix = 0; ix < dimx; ix++)
	  data[iy*dimx+ix] = (*this)[dimx*dimy*iz + dimx*iy + ix];
    }
    void get_sheet2(std::size_t iz, arr::v2D<T> &data)
    {
      data.resize2D(dimx,dimy);
      get_sheet(iz,data);
    }
    void get_sheetX2(std::size_t ix, arr::v2D<T> &data)
    {
      data.resize2D(dimy,dimz);
      get_sheetX(ix,data);
    }
    void get_sheetY2(std::size_t iy, arr::v2D<T> &data)
    {
      data.resize2D(dimx,dimz);
      get_sheetY(iy,data);
    }
    void get_sheetZ2(std::size_t iz, arr::v2D<T> &data)
    {
      data.resize2D(dimx,dimy);
      get_sheetZ(iz,data);
    }

    void get_cube(std::vector<T> &data)
    {
      data.resize(dimx*dimy*dimz);
      for (std::size_t iz = 0; iz < dimz; iz++)
	for (std::size_t iy = 0; iy < dimy; iy++)
	  for (std::size_t ix = 0; ix < dimx; ix++)
	    data[dimx*dimy*iz + iy*dimx + ix] = (*this)[dimx*dimy*iz + dimx*iy + ix];
    }
  private:
    std::size_t dimx, dimy, dimz;
    //unsigned long dimx, dimy, dimz;
    //std::vector<T> v;
  };

} // namespace arr

#endif
