// create turbulence velocity field
// Philipp Girichidis, Nov 2010
// last change: 10 Dec 2010

#include <iostream>
#include <fstream>
#include "vector-ndim.hh"
#include <cmath>
#include <complex>
#include <fftw3.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <hdf5.h>
#include "HDFIO.h"

#ifdef CREATE_TURB_DEBUG
#include "histogram.h"
#endif

const double pi = 3.141592653589793238462643383279502;

//###############################
//
// HELPER FUNCTIONS
//
//###############################

double power_spectrum(double k, double exponent)
{
  if(k<1e-6) return 0.0;
  else return pow(k,exponent);
}

void random_uniform(std::size_t N, std::vector<double> &data, unsigned long int seed=0)
{
  data.resize(N);

  gsl_rng * r;
  const gsl_rng_type * T;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
  gsl_rng_set(r,seed);

  for(std::size_t i = 0; i<N; i++)
    {
      data[i] = gsl_rng_uniform_pos(r);
    }
  gsl_rng_free(r);
}

void random_gaussian(std::size_t N, std::vector<double> &data, unsigned long int seed=0, double sigma=1.0)
{
  data.resize(N);

  gsl_rng * r;
  const gsl_rng_type * T;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
  gsl_rng_set(r,seed);

  for(std::size_t i = 0; i<N; i++)
    {
      data[i] = gsl_ran_gaussian(r,sigma);
    }
  gsl_rng_free(r);
}

void random_uniform3D(std::size_t dim, std::vector<double> &data, unsigned long int seed=0)
{
  random_uniform(dim*dim*dim, data, seed);
}

void random_gaussian3D(std::size_t dim, std::vector<double> &data, unsigned long int seed=0, double sigma=1.0)
{
  random_gaussian(dim*dim*dim, data, seed);
}


//###############################
//
// MAIN PROGRAMME
//
//###############################

int main(int argc, char* argv[])
{

  // general simulation parameters
  size_t N                         = 7;
  long   dim                       = pow(2,N);
  double kmin                      = 1.0;
  // do not use kmax = dim/2-1 (Nyquist modes = 1/2 f_s)
  double kmax                      = (double)(dim/2-1);
  double ps_exp                    = -4.0;
  double v_norm_stddev             = 1.0;

  // random number parameters
  double ran_sigma                 = 1.0;
  unsigned long ran_seed           = 0;
  unsigned long ran_seed_offset[3] = {0,17,23};
  bool ran_symmetric               = false;
  bool random_phase                = true;
  bool random_amplitude            = false;

  // output & print parameters
  std::string outfile_base         = "vel3D";
  std::string output_type          = "double precision";
  bool print_k_slice               = true;
  bool print_v_slice               = true;
  std::string dim_str[3]           = {"x","y","z"};
  

  // analyse command line parameters
  for(int i=1; i<argc; i++)
    {
      std::string param = argv[i];
      if(param.substr(0,5) == "kmin=")
	{
	  kmin = atof((param.substr(5)).c_str());
	  std::cout << "  CHANGED kmin to " << kmin << std::endl;
	}
      if(param.substr(0,5) == "kmax=")
	{
	  kmax = atof((param.substr(5)).c_str());
	  if(kmax > dim-2)
	    {
	      std::cout << "  !! kmax must be smaller than dim-1 (no Nyquist frequencies)" << std::endl;
	      kmax = dim-2;
	    }
	  std::cout << "  CHANGED kmax to " << kmax << std::endl;
	  std::cout << "    hint: if you change dim afterwards, kmax may be too large!" << std::endl;
	}
      if(param.substr(0,6) == "sigma=")
	{
	  ran_sigma = atof((param.substr(6)).c_str());
	  std::cout << "  CHANGED random spread to sigma = " << ran_sigma << std::endl;
	}
      if(param.substr(0,2) == "d=")
	{
	  dim = atoi((param.substr(2)).c_str());
	  std::cout << "  CHANGED dim to " << dim << std::endl;
	}
      if(param.substr(0,2) == "p=")
	{
	  ps_exp = atof((param.substr(2)).c_str());
	  std::cout << "  CHANGED power spectrum exponent to " << ps_exp << std::endl;
	}
      if(param.substr(0,6) == "burger")
	{
	  ps_exp = -2.0;
	  std::cout << "  CHANGED power spectrum Burger spectrum, p = " << ps_exp << std::endl;
	}
      if(param.substr(0,10) == "kolmogorov")
	{
	  ps_exp = -11.0/6.0;
	  std::cout << "  CHANGED power spectrum Kolmogorov spectrum, p = -11/6 = " << ps_exp << std::endl;
	}
      if(param.substr(0,2) == "p=")
	{
	  ps_exp = atof((param.substr(2)).c_str());
	  std::cout << "  CHANGED power spectrum exponent to " << ps_exp << std::endl;
	}
      if(param.substr(0,5) == "seed=")
	{
	  ran_seed = atoi((param.substr(5)).c_str());
	  std::cout << "  CHANGED random seed to " << ran_seed << std::endl;
	}
      if(param.substr(0,2) == "o=")
	{
	  outfile_base = (param.substr(2)).c_str();
	  std::cout << "  CHANGED output file base name to " << outfile_base << std::endl;
	}
      if(param.substr(0,3) == "flt")
	{
	  output_type = "single precision";
	  std::cout << "  CHANGED output data type to float" << std::endl;
	}
      if(param.substr(0,4) == "symm")
	{
	  ran_symmetric = true;
	  std::cout << "  CHANGED to symmetric velocity field" << std::endl;
	}
      if(param.substr(0,6) == "norand")
	{
	  random_phase     = false;
	  random_amplitude = false;
	  std::cout << "  CHANGED to no random numbers" << std::endl;
	}
      if(param.substr(0,7) == "randamp")
	{
	  random_amplitude = true;
	  std::cout << "  CHANGED to random amplitude" << std::endl;
	}
      if(param.substr(0,6) == "vnorm=")
	{
	  v_norm_stddev = atof((param.substr(6)).c_str());
	  std::cout << "  CHANGED velocity std deviation normalisation v_norm = " << v_norm_stddev << std::endl;
	}
      if(param.substr(0,8) == "nokslice")
	{
	  print_k_slice = false;
	  std::cout << "  CHANGED: do not print k slice file!" << std::endl;
	}
      if(param.substr(0,8) == "novslice")
	{
	  print_v_slice = false;
	  std::cout << "  CHANGED: do not print velocity slice files!" << std::endl;
	}
      if(param.substr(0,8) == "noslices")
	{
	  print_v_slice = false;
	  print_k_slice = false;
	  std::cout << "  CHANGED: do not print any slice files!" << std::endl;
	}
    }
  size_t Nelements = dim*dim*dim;
  std::cout << std::endl;


  // write information to info file
  std::cout << "  WRITING parameters to file " << outfile_base + ".info" << std::endl;
  std::fstream finfo((outfile_base + ".info").c_str(), std::ios::out);
  finfo << " file basename      : " << outfile_base << std::endl;
  finfo << " data per direction : " << dim << std::endl;
  finfo << " total data per dir : " << Nelements << std::endl;
  finfo << " total data         : " << 3*Nelements << std::endl;
  finfo << " data type          : " << output_type << std::endl;
  finfo << " file size [MB]     : " << (output_type == "double precision" ?
					3*Nelements*sizeof(double)/1024/1024 :
					3*Nelements*sizeof(float)/1024/1024 ) << std::endl;
  finfo << " kmin               : " << kmin << std::endl;
  finfo << " kmax               : " << kmax << std::endl;
  finfo << " power spec indx    : " << ps_exp << std::endl;
  finfo << " init seed          : " << ran_seed << std::endl;
  finfo << " seed dimension 1   : " << ran_seed + ran_seed_offset[0] << std::endl;
  finfo << " seed dimension 2   : " << ran_seed + ran_seed_offset[1] << std::endl;
  finfo << " seed dimension 3   : " << ran_seed + ran_seed_offset[2] << std::endl;
  finfo << " symmetric field    : " << ran_symmetric << std::endl;
  finfo << " random phase       : " << random_phase << std::endl;
  finfo << " vel std dev norm   : " << v_norm_stddev << std::endl;
  finfo.close();


  // prepare vectors
  arr::v3D<double> k3D(dim,0.0);
  arr::v3D<double> vel_r(dim);
  arr::v3D<double> vel_i(dim);

  // set value according to power spectrum
  double kmin_test = 2.0;
  double kmax_test = -1.0;
  long end = dim/2;

#ifdef CREATE_TURB_DEBUG
  arr::v1D<double> kbins(dim,0.0);
#endif

  // calculate k-vectors for entire k cube
  // with k_000 in the centre of the cube.
  // store it shifted, so that k_000 is at 000.
//   for(int iz=-dim/2+1; iz<end; iz++)
//     for(int iy=-dim/2+1; iy<end; iy++)
//       for(int ix=-dim/2+1; ix<end; ix++)
// 	{
// 	  double length = sqrt(pow((double)ix,2.0)+
// 			       pow((double)iy,2.0)+
// 			       pow((double)iz,2.0));
// 	  if((length >= kmin-1e-6) && (length <= kmax+1e-6))

// 	    {
// 	      if(length > kmax_test) kmax_test = length;
// 	      if(length < kmin_test) kmin_test = length;
	      
// 	      k3D((ix+dim)%dim, (iy+dim)%dim, (iz+dim)%dim) = power_spectrum(length,ps_exp);
// 	    }
// 	}
//   std::cout << "  CHECKED kmin / kmax : [" << kmin_test << ";" << kmax_test << "]" << std::endl;

  // calculate k-vectors for one corner of the k cube
  // k-vectors are located like index numbers
  //   i.e. k_000 is at position [0][0][0]
  // up to half of the cube (Nyquist-k)
  // after that negative k parts
  //
  // storage picture
  // 
  //  - X = k_000
  // /
  // X 1 2 3 0 3 2 1
  // 1 2 3 4 0 4 3 2
  // 2 3 4 5 0 5 4 3
  // 3 4 5 6 0 6 5 4
  // 0 0 0 0 0 0 0 0 |Nyquist "row"
  // 3 4 5 6 0 6 5 4
  // 2 3 4 5 0 5 4 3
  // 1 2 3 4 0 4 3 2
  // k_pos  |N|k_neg
  //         \_Nyquist "column"

  for(long iz=0; iz<end; iz++)
    for(long iy=0; iy<end; iy++)
      for(long ix=0; ix<end; ix++)
	{
	  double length = sqrt(pow((double)ix,2.0)+
			       pow((double)iy,2.0)+
			       pow((double)iz,2.0));

	  if((length >= kmin-1e-6) && (length <= kmax+1e-6))
	    {
	      if(length > kmax_test) kmax_test = length;
 	      if(length < kmin_test) kmin_test = length;
	      k3D(ix,iy,iz) = power_spectrum(length,ps_exp);
	    }
#ifdef CREATE_TURB_DEBUG
	  kbins[(long)round(length)] += k3D(ix,iy,iz);
#endif

	}
  std::cout << "  CHECKED kmin / kmax : [" << kmin_test << ";" << kmax_test << "]" << std::endl;

  // mirror corner to all 7 other corners
  k3D.mirrorX(dim/2.0);
  k3D.mirrorY(dim/2.0);
  k3D.mirrorZ(dim/2.0);


#ifdef CREATE_TURB_DEBUG
  std::cout << "  DEBUG: WRITING initial k spectrum to file \"" 
	    << outfile_base + "-kspec.dat\"" << std::endl;
  std::fstream kspec((outfile_base + "-kspec.dat").c_str(), std::ios::out);
  kbins.print_dat_form(kspec);
  kspec.close();
#endif

  // Fourier transform
  // using fftw library
  // prepare fftw data arrays and fftw plan
  fftw_complex *in, *out;
  fftw_plan p;
  in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * k3D.size());
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * k3D.size());
  p   = fftw_plan_dft_3d(dim, dim, dim, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    

  // prepare data arrays
  arr::v3D<double> rand_norm(dim,1.0);
  arr::v3D<double> rand_unif(dim,1.0);
  arr::v3D<double> ran_value_r(dim,1.0);
  arr::v3D<double> ran_value_i(dim,1.0);
  arr::v3D<double> ran_phase(dim,1.0);


  // prepare output file
  std::fstream fout((outfile_base + ".bin").c_str(), std::ios::out | std::ios::binary);
  std::cout << "  WRITING data to file " << outfile_base + ".bin" << std::endl;


  // do three transforms with different seeds for three velocities velx, vely, velz
  for(unsigned int dimension = 0; dimension < 3; dimension++)
    {
      std::cout << std::endl;
      std::cout << "  CREATING velocity field for dimension " << dimension+1 
		<< " with seed " << ran_seed + ran_seed_offset[dimension] << std::endl;


      // fill k-data into input array, including random numbers for phases and amplitudes

      if(random_phase)
	{
	  // create random numbers
	  random_uniform3D (dim, rand_unif, ran_seed + ran_seed_offset[dimension]);
	  // symmetrise the field ...
	  rand_unif.shift(1,dim-1,1,dim-1,1,dim/2-1,0,0,dim/2);
	  rand_unif.reverse3D(1,dim-1,1,dim-1,dim/2+1,dim-1);

	  // ... and the slices with one index = 0
	  // slice z=0
	  rand_unif.shift(0,dim-1,1,dim/2,0,0,0,dim/2,0);
	  rand_unif.reverse3D(1,dim-1,dim/2+1,dim-1,0,0);
	  // slice x=0
	  rand_unif.shift(0,0,0,dim-1,1,dim/2,0,0,dim/2);
	  rand_unif.reverse3D(0,0,1,dim-1,dim/2+1,dim-1);
	  // slice y=0
	  rand_unif.shift(0,dim-1,0,0,1,dim/2,0,0,dim/2);
	  rand_unif.reverse3D(1,dim-1,0,0,dim/2+1,dim-1);

	  // ... and axis with two indices = 0
	  for(long i=1; i<dim/2; i++)
	    {
	      rand_unif(0,0,i) = rand_unif(0,0,dim-i);
	      rand_unif(0,i,0) = rand_unif(0,dim-i,0);
	      rand_unif(i,0,0) = rand_unif(dim-i,0,0);
	    }
	}

      if(random_amplitude)
	{
	  // generate random amplitude
	  random_gaussian3D(dim, rand_norm, ran_seed + ran_seed_offset[dimension], ran_sigma);
	  rand_norm.set(1.0);

	  // symmetrise the field ...
	  rand_norm.shift(1,dim-1,1,dim-1,1,dim/2-1,0,0,dim/2);
	  rand_norm.reverse3D(1,dim-1,1,dim-1,dim/2+1,dim-1);

	  // slice z=0
	  rand_norm.shift(0,dim-1,1,dim/2,0,0,0,dim/2,0);
	  rand_norm.reverse3D(1,dim-1,dim/2+1,dim-1,0,0);
	  // slice x=0
	  rand_norm.shift(0,0,0,dim-1,1,dim/2,0,0,dim/2);
	  rand_norm.reverse3D(0,0,1,dim-1,dim/2+1,dim-1);
	  // slice y=0
	  rand_norm.shift(0,dim-1,0,0,1,dim/2,0,0,dim/2);
	  rand_norm.reverse3D(1,dim-1,0,0,dim/2+1,dim-1);

	  // ... and axis with two indices = 0
	  for(long i=1; i<dim/2; i++)
	    {
	      rand_norm(0,0,i) = rand_norm(0,0,dim-i);
	      rand_norm(0,i,0) = rand_norm(0,dim-i,0);
	      rand_norm(i,0,0) = rand_norm(dim-i,0,0);
	    }

	  // gaussian distribution around k-vector value
	  arr::mult(ran_value_r,rand_norm,k3D);
	  arr::mult(ran_value_i,rand_norm,k3D);
	}
      else
	{
	  ran_value_r = k3D;
	  ran_value_i = k3D;
	}

      // if symmetric, then mirror in the centre of the array
      if(ran_symmetric)
	{
	  rand_norm.mirrorX(dim/2.0);
	  rand_norm.mirrorY(dim/2.0);
	  rand_norm.mirrorZ(dim/2.0);
	  rand_unif.mirrorX(dim/2.0);
	  rand_unif.mirrorY(dim/2.0);
	  rand_unif.mirrorZ(dim/2.0);
	  
	  if(false)
	    for(long i=0; i<dim; i++)
	      for(long j=0; j<dim; j++)
		{
		  rand_norm(0,i,j) = 0.0;
		  rand_norm(j,i,0) = 0.0;
		  rand_norm(i,0,j) = 0.0;
		  rand_unif(0,i,j) = 0.0;
		  rand_unif(j,i,0) = 0.0;
		  rand_unif(i,0,j) = 0.0;
		}
	}
      

      // set half of imaginary parts to negative
      ran_value_i.mult3D(1,dim-1,1,dim-1,dim/2+1,dim-1,-1.0);
      // ... including slices with one index = 0
      // ... including axis
      ran_value_i.mult3D(0,dim-1,dim/2+1,dim-1,0,0,-1.0);
      ran_value_i.mult3D(dim/2+1,dim-1,0,0,0,dim-1,-1.0);
      ran_value_i.mult3D(0,0,0,dim-1,dim/2*1,dim-1,-1.0);
      // uniformly distributed random phase
      arr::mult(ran_phase,rand_unif,2.0*pi);

      ran_value_i.print3D();

      for(std::size_t i = 0; i<k3D.size(); i++)
	{
	  in[i][0] = ran_value_r[i] * cos(ran_phase[i]);
	  in[i][1] = ran_value_i[i] * sin(ran_phase[i]);
	}


      // do fourier transformation
      fftw_execute(p);
      

      // get output data in more comfortable array
      for(std::size_t i = 0; i<k3D.size(); i++)
	{
	  vel_r(i) = out[i][0]/sqrt((double)Nelements);
	  vel_i(i) = out[i][1]/sqrt((double)Nelements);
	}

      
      // analyse fourier transformed field and normalise it
      double minr, maxr, mini, maxi, stddevr, meanr, skewr;
      vel_r.minmax(minr, maxr);
      vel_i.minmax(mini, maxi);
      arr::statist_moments(vel_r, meanr, stddevr, skewr);
      std::cout << "  VEL MIN/MAX real : " << minr << " " << maxr << std::endl;
      std::cout << "              imag : " << mini << " " << maxi << std::endl;
      std::cout << "              mean : " << meanr << std::endl;
      std::cout << "            stddev : " << stddevr << std::endl;
      std::cout << "          skewness : " << skewr << std::endl;

#ifdef CREATE_TURB_DEBUG
      histogram HST;
      HST.set(&vel_r.front(),vel_r.size(),60);
      HST.plot();
#endif

      vel_r.mult(v_norm_stddev/stddevr);
      stddevr = arr::stddev(vel_r, meanr);
      std::cout << "  NEW  mean/stddev : " << meanr << " " << stddevr << std::endl;
      

      // write data to binary output file
      if(output_type == "double precision")
	{
	  std::size_t SIZE = sizeof(out[0][0]);
	  for(std::size_t i=0; i<k3D.size(); i++)
	    fout.write((char*) &out[i][0], SIZE);
	}
      else
	{
	  float dummy;
	  std::size_t SIZE = sizeof(dummy);
	  for(std::size_t i=0; i<k3D.size(); i++)
	    {
	      dummy = (float)out[i][0];
	      fout.write((char*) &dummy, SIZE);
	    }
	}

      /// write HDF5 output
      HDFIO HDFOutput = HDFIO();
      std::string hdf5filename;
      std::string varname = "vel" + dim_str[dimension];
      hdf5filename = outfile_base + "_" + varname;
      HDFOutput.create(hdf5filename);
      /// write data
      std::vector<int> hdf5dims(3);
      hdf5dims[2] = dim;
      hdf5dims[1] = dim;
      hdf5dims[0] = dim;
      float * flaotfield = new float[vel_r.size()];
      for (unsigned long n = 0; n < vel_r.size(); n++) flaotfield[n] = vel_r[n];
      HDFOutput.write(flaotfield, varname, hdf5dims, H5T_NATIVE_FLOAT);
      HDFOutput.close();
      delete [] flaotfield;
      
      
    }
  

  // close output file
  fout.close();

  std::cout << std::endl;
  if(print_k_slice)
    {
      // get first slice and store in file
      std::cout << "  WRITING k slice 1 to file \"" 
		<< outfile_base + "-kslice.dat" << "\"" << std::endl;
      arr::v2D<double> kslice(dim);
      k3D.get_sheet2(1,kslice);
      std::fstream kfile((outfile_base + "-kslice.dat").c_str(), std::ios::out);
      kslice.print_dat_form(kfile);
      kfile.close();
    }
  if(print_v_slice)
    {
      arr::v2D<double> vslice(dim);
      std::fstream vfile;

      // X slice
      std::cout << "  WRITING vel_x slice dim/2 to file \"" 
		<< outfile_base + "-vsliceX.dat" << "\"" << std::endl;
      vel_r.get_sheetX2(dim/2,vslice);
      vfile.open((outfile_base + "-vsliceX.dat").c_str(),std::ios::out);
      vslice.print_dat_form(vfile);
      vfile.close();

      // Y slice
      std::cout << "  WRITING vel_y slice dim/2 to file \"" 
		<< outfile_base + "-vsliceY.dat" << "\"" << std::endl;
      vel_r.get_sheetY2(dim/2,vslice);
      vfile.open((outfile_base + "-vsliceY.dat").c_str(),std::ios::out);
      vslice.print_dat_form(vfile);
      vfile.close();
      
      // Yslice
      std::cout << "  WRITING vel_z slice dim/2 to file \"" 
		<< outfile_base + "-vsliceZ.dat" << "\"" << std::endl;
      vel_r.get_sheetZ2(dim/2,vslice);
      vfile.open((outfile_base + "-vsliceZ.dat").c_str(),std::ios::out);
      vslice.print_dat_form(vfile);
      vfile.close();
    }


  // clear fftw memory
  fftw_destroy_plan(p);
  fftw_free(in);
  fftw_free(out);

  return 0;
}
