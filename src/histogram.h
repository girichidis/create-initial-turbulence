// histogram
// Philipp Girichidis, 2009
// last change, 06.12.2010

#ifndef _HISTOGRAM_H_
#define _HISTOGRAM_H_

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>

class histogram
{
 public:
  histogram(int norm = 400)
    {
      data_allocated = false;
      normalization = norm;
      log_data = false;
      log_bin = false;
      datarange_auto = true;
    }
  histogram(bool logx, bool logy, int norm = 400)
    {
      data_allocated = false;
      normalization = norm;
      log_data = logx;
      log_bin = logy;
      datarange_auto = true;
    }
  ~histogram()
    {
      if(data_allocated)
	{
	  delete[] bin;
	  delete[] binnorm;
	  delete[] bnds;
	  delete[] logbin;
	}
    }
  void set_min_max(double dmin, double dmax)
  {
    datarange_auto = false;
    datamin = dmin;
    datamax = dmax;
    std::cout << " data range set to [" << dmin << ":" << dmax << "]" << std::endl;
  }
  void set(double data[], int n, int bins)
    {
      data_allocated = true;
      Nbins = bins;
      Ndata = n;
      
      // check if less data than normalization
      if(n < normalization) normalization = n;

      if(datarange_auto)
	{
	  // find miniumum and maximum of the dataset
	  datamax = data[0];
	  datamin = data[0];
	  for (int i=0; i<Ndata; i++)
	    {
	      if(data[i] > datamax) datamax = data[i];
	      if(data[i] < datamin) datamin = data[i];
	    }
	  // increase maximum a little bit so that the max of the
	  // last boundary is slightly bigger than real datamax
	  // if max of last boundary == datamax then datamax
	  // would be set to the next bin: [bnd_min; bnd_max)
	  // increase by tiny*binwidth
	  double tiny = 1e-4;
	  double olddatamax = datamax;
	  if(log_data)
	    datamax *= pow(10.0,tiny*(log10(datamax/datamin)/(double)Nbins));
	  else
	    datamax += tiny*(datamax-datamin)/(double)Nbins;
	  std::cout << "# auto range data set dmax from " 
		    << olddatamax << " to " << datamax << std::endl;
	}
      
      // allocate memory
      bnds    = new double[Nbins+1];
      bin     = new int[Nbins];
      binnorm = new int[Nbins];
      logbin  = new double[Nbins];

      // set bnds of the bins and put data into container
      for (int i=0; i<Nbins; i++)
	{
	  bin[i] = 0;
	  binnorm[i] = 0;
	}
      if(log_data)
	{
	  // set boundaries
	  double delta0 = (log10(datamax)-log10(datamin));
	  double delta = delta0/(double)Nbins;
	  // ???double delta1 = delta0/(double)(Nbins-1);
	  for (int i = 0; i<= Nbins; i++)
	    {
	      bnds[i] = datamin*pow(10.0,(double)i*delta);
	    }
	  // put data into bins
	  for (int i=0; i<Ndata; i++)
	    {
	      // ???int binnumber = floor((log10(data[i])-log10(datamin))/delta1 + 0.5);
	      int binnumber = (int)((log10(data[i])-log10(datamin))/delta);
	      //std::cout << "binnr:" << binnummer << std::endl;
	      if((binnumber >= 0) && (binnumber < Nbins))
		bin[binnumber]++;
	    }
	}
      else
	{
	  // set boundaries
	  double delta0 = (datamax-datamin);
	  double delta = delta0/(double)Nbins;
	  // ???double delta1 = delta0/(double)(Nbins-1);
	  for (int i = 0; i<= Nbins; i++)
	    {
	      bnds[i] = (double)i*delta+datamin;
	    }
	  //put data into bins
	  for (int i=0; i<Ndata; i++)
	    {
	      // ???int binnumber = floor((data[i]-datamin)/delta1 + 0.5);
	      int binnumber = (int)((data[i]-datamin)/delta);
	      if((binnumber >= 0) && (binnumber < Nbins))
		bin[binnumber]++;
	    }  
	}

      if(log_bin)
	{
	  log_norm_factor = 0.0;
	  for(int i=0; i<Nbins; i++)
	    {
	      logbin[i] = log10(bin[i]);
	      if(logbin[i] > 0.0) log_norm_factor += logbin[i];
	    }
	  log_norm_factor /= (double)normalization;
	  log_norm_factor = 1.0/log_norm_factor;
	}

      //for (int i=0; i<Nbins; i++) std::cout << bin[i] << std::endl;
      // normalize bins, find min and max
      binnormmax = 0;
      binnormmin = Ndata;
      binnormavg = 0.0;
      binmax = 0;
      binmin = Ndata;
      binavg = 0.0;
      for (int i=0; i<Nbins; i++)
	{
	  if(log_bin)
	    {
	      binnorm[i] = (int)(logbin[i]*log_norm_factor);
	      //(double)normalization/log10((double)Ndata));
	    }
	  else
	    {
	      binnorm[i] = bin[i]*normalization/Ndata;
	    }
	  if (binnorm[i] > binnormmax) binnormmax = binnorm[i];
	  if (binnorm[i] < binnormmin) binnormmin = binnorm[i];
	  binnormavg += (double)bin[i];
	  if (bin[i] > binmax) binmax = bin[i];
	  if (bin[i] < binmin) binmin = bin[i];
	  binavg += (double)bin[i];
	}
      binavg /= (double)Nbins;
    }

  int get_bin(int n)
  {
    int result = 0;
    if(n>-1 && n<Nbins)
      result = bin[n];
    else
      {
	std::cout << "Wrong bin index!!!" << std::endl;
      }
    return result;
  }

  double get_binnorm(int n)
  {
    double result = 0.0;
    if(n>-1 && n<Nbins)
      result = binnorm[n];
    else
      {
	std::cout << "Wrong bin index!!!" << std::endl;
      }
    return result;
  }

  double get_bnds(int n)
  {
    double result = 0.0;
    if(n>-1 && n<Nbins+1)
      result = bnds[n];
    else
      {
	std::cout << "Wrong bin index!!!" << std::endl;
      }
    return result;
  }

  double get_mid_bnds(int n)
  {
    double result = 0.0;
    if(n>-1 && n<Nbins)
      result = 0.5*(bnds[n+1]+bnds[n]);
    else
      {
	std::cout << "Wrong bin index!!!" << std::endl;
      }
    return result;
  }

  void plotinfo(std::ostream &out = std::cout)
  {
    out << "# Number of Bins: " << Nbins << std::endl;
    out << "#  bin minimum  : " << binmin << std::endl;
    out << "#  bin maximum  : " << binmax << std::endl;
    out << "#  bin average  : " << binavg << std::endl;
    out << "# Number of Data: " << Ndata << std::endl;
    out << "#  minimum value: " << datamin << std::endl;
    out << "#  maximum value: " << datamax << std::endl;
    out << "# Normalization : " << normalization << std::endl;
  }

  void plot(std::ostream &out = std::cout, bool vertical=true, bool info = true)
  {
    if(info)
      { 
	plotinfo(out);
      }
    if(vertical)
      {
	int space = 12;
	out << "#  " << std::setw(space) << "^" << std::endl;
	for(int i = binnormmax; i>=0; i--)
	  {
	    if(log_bin)
	      out << "#" 
		  << std::setw(space) 
		  << (i+1)*log10(binmax)/log_norm_factor;
	    else
	      out << "#" 
		  << std::setw(space) 
		  << (i+1)*Ndata/normalization;
	    out << " | ";
	    for(int j = 0; j<Nbins; j++)
	      {
		if(binnorm[j] <= i) out << " ";
		else out << "X";
	      }
	    out << std::endl;
	  }
	out << "#  " << std::setw(space) << "+";
	for(int j = 0; j<Nbins+2; j++) out << "-";
	out << ">" << std::endl;
      }
    else
      {
	// plot the data
	for (int i=0; i<Nbins; i++)
	  {
	    for (int j=1; j<(bin[i]); j++) out << '*';
	    out << std::endl;
	  }
      }
  }

  void plotdata(std::ostream &out = std::cout)
  {
    plotinfo(out);
    for(int i=0; i<Nbins; i++)
      {
	out << std::setw(12) << bnds[i];
	out << std::setw(12) << bnds[i+1];
	if(log_bin) out << std::setw(12) << log10(bin[i]) << std::endl;
	else out << std::setw(12) << bin[i] << std::endl;
      }
  }

  void plotdata(std::string filename)
  {
    std::fstream file;
    file.open(filename.c_str(), std::ios::out);
    plotinfo(file);
    for(int i=0; i<Nbins; i++)
      {
	file << std::setw(12) << bnds[i];
	file << std::setw(12) << bnds[i+1];
	file << std::setw(12) << bin[i] << std::endl;
      }
    file.close();
  }
  
  void gnuplot(std::ostream &out)
  // creates a gnuplot file
  {
    
  }

 private:
  // mode variables
  bool log_data;
  bool log_bin;
  bool datarange_auto;

  // visualization
  int    normalization;
  double log_norm_factor;

  // data variables
  bool   data_allocated;
  double *bnds;
  int    *bin;
  int    *binnorm;
  double *logbin;
  int    Nbins;
  int    Ndata;

  // min and max vars
  int    binnormmax;
  int    binnormmin;
  double binnormavg;
  int    binmax;
  int    binmin;
  double binavg;
  double datamax;
  double datamin;
};

#endif
