// simple program to test binary turbulence file

// Philipp Girichidis, Nov 2010
// last change: 14 Dec 2010

#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <algorithm>
#include <vector>

using namespace std;

const double pi  = 3.14159265;
const double G   = 6.67428e-8;
const double kyr = 3.1536e10;

// plummer coefficients: rho_core, R_core, eta
double plummer_sets[9][3] =
  {
    {  2.79207e-14,    2.992e+14,  1.5},  //       20 AU
    {  9.87289e-15,    5.984e+14,  1.5},  //       40 AU
    {  3.49199e-15,   1.1968e+15,  1.5},  //       80 AU
    {  1.23597e-15,   2.3936e+15,  1.5},  //      160 AU
    {  4.38292e-16,   4.7872e+15,  1.5},  //      320 AU
    {  1.56207e-16,   9.5744e+15,  1.5},  //      640 AU
    {  5.63963e-17,  1.91488e+16,  1.5},  //     1280 AU
    {  2.10244e-17,  3.82976e+16,  1.5},  //     2560 AU
    {  1.21702e-17,  5.74464e+16,  1.5}   //     3840 AU
  };

double plummer(double r, double R_c, double rho_c, double eta)
{
  return rho_c*pow(R_c/sqrt(R_c*R_c+r*r),eta);
}

double powerlaw(double r, double R0, double rho0, double p)
{
  return rho0 * pow(r/R0,p);
}

double max_radius = 3e17;
double min_radius = 0.0;//5e17;
double box_scale = 8e17;
double max_radius_norm = max_radius/box_scale;
double min_radius_norm = min_radius/box_scale;
double vel_scale = 8.94e4;
double dx = 0.0;

int main(int argc, char* argv[])
{
  cout << endl;
  cout << "*****************************" << endl;
  cout << argv[0] << endl;
  cout << "*****************************" << endl;
  cout << endl;
  // read command line parameters
  if(argc < 3)
    {
      cout << "usage :  " << argv[0] << "   vel-file   N-per-dim   [ plu=n ]  [ c=compr ] [ v=vel-scale ] [ d=NNmax ] [ dbl ]  [ dens=file ]" << endl;
      cout << endl;
      cout << "      plu=n       : gives the predefined plummer spheres (1..9)" << endl;
      cout << "                    choosing 0 uses no density weighting" << endl;
      cout << "      c=compr     : print compression ratio (hydro/gravity)" << endl;
      cout << "                    N : slice N from the cube N in [1..N-per-dim-2]" << endl;
      cout << "                    a : integrated along axis a, where a in {x,y,z}" << endl;
      cout << endl;
      cout << "      v=vel-scale : scale factor of the velocity. default value: " << vel_scale << endl;
      cout << endl;
      cout << "      d=NNmax     : sort NNmax largest 3D divergence values and plot inverse." << endl;
      cout << endl;
      cout << "      dbl         : read double precision from file" << endl;
      cout << endl;
      cout << "      dens=file   : binary file for density (the same type/dimension as vel-file" << endl;
      cout << endl;
      exit(1);
    }
  unsigned int dim   = atoi(argv[2]);
  int       plu_set  = 0;
  string    splu_set = "";
  string outfilename = argv[1];
  string comprfname  = argv[1];
  string      scompr = "";
  int         icompr = 0;
  dx = box_scale/(double)dim;
  int    NNmax       = 0;
  bool double_data   = false;
  string densfile    = "";

  double rho_max     = 0.0;

  // analyse optional parameters
  for(int i=3; i<argc; i++)
    {
      string param = argv[i];
      if(param.substr(0,5) == "dens=")
        {
          densfile = param.substr(5);
          cout << "  DENSITY: USE DATA FROM FILE : " << densfile << endl;
        }
      if(param.substr(0,4) == "plu=")
        {
          plu_set  = atoi((param.substr(4)).c_str());
	  splu_set = param.substr(4);
          cout << "  DENSITY: USE PLUMMER SPHERE : " << plu_set << endl;
        }
      if(param.substr(0,2) == "v=")
        {
          vel_scale = atof((param.substr(2)).c_str());
          cout << "  CHANGED VEL_SCALE TO : " << vel_scale << endl;
        }
      if(param.substr(0,2) == "d=")
        {
          NNmax = atoi((param.substr(2)).c_str());
          cout << "  FIND " << NNmax << " highest divergence numbers" << endl;
        }
      if(param.substr(0,2) == "c=")
        {
          if(param[2] == 'x')      icompr = -1;
          else if(param[2] == 'y') icompr = -2;
          else if(param[2] == 'z') icompr = -3;
          else                     icompr = atoi((param.substr(2)).c_str());
          cout << "  COMPUTE COMPRESSION : " << icompr << endl;
	  scompr = param.substr(2);
        }
      if(param.substr(0,3) == "dbl")
        double_data = true;
    }

  if(densfile == "")
    {
      outfilename += "-divergence-PLU-R"  + splu_set + ".dat";
      comprfname  += "-compr-ratio-" + scompr + "-PLU-R" + splu_set + ".dat";
    }
  else
    {
      outfilename += "-divergence.dat";
      comprfname  += "-compr-ratio-" + scompr + ".dat";
    }
  long NN = dim*dim*dim;
  if(double_data)
    cout << "  processing " << dim << "^3 = " << NN << " double numbers for 3 dimensions!" << endl;
  else
    cout << "  processing " << dim << "^3 = " << NN << " float numbers for 3 dimensions!" << endl;

  // create data arrays
  std::vector<float>  velx(NN);
  std::vector<float>  vely(NN);
  std::vector<float>  velz(NN);
  std::vector<float>  vrad(NN);
  std::vector<float>  divx(NN);
  std::vector<float>  divy(NN);
  std::vector<float>  divz(NN);
  std::vector<double> compr(NN);
  std::vector<float>  divxyz(NN);
  std::vector<float>  dens;
  if(densfile != "") dens.assign(NN,0.0);

  int SIZE = sizeof(velx[0]);

  cout << "  read velocity from file : \"" << argv[1] << "\" ... ";
  fstream file(argv[1],ios::in | ios::binary);
  if(double_data)
    {
      double dummy;
      int dSIZE = sizeof(dummy);
      for(int i=0; i<NN; i++)
        {
          file.read(reinterpret_cast<char *>(&dummy), dSIZE);
          velx[i] = dummy;
        }
      for(int i=0; i<NN; i++)
        {
          file.read(reinterpret_cast<char *>(&dummy), dSIZE);
          vely[i] = dummy;
        }
      for(int i=0; i<NN; i++)
        {
          file.read(reinterpret_cast<char *>(&dummy), dSIZE);
          velz[i] = dummy;
        }
    }
  else
    {
      for(int i=0; i<NN; i++)
        file.read(reinterpret_cast<char *>(&velx[i]), SIZE);
      for(int i=0; i<NN; i++)
        file.read(reinterpret_cast<char *>(&vely[i]), SIZE);
      for(int i=0; i<NN; i++)
        file.read(reinterpret_cast<char *>(&velz[i]), SIZE);
    }
  file.close();
  cout << "done!" << endl;

  if(densfile != "")
    {
      cout << "  read density from file : \"" << densfile << "\" ... ";
      fstream fdens(densfile.c_str(),ios::in | ios::binary);
      if(double_data)
        {
          double dummy;
          int dSIZE = sizeof(dummy);
          for(int i=0; i<NN; i++)
            {
              fdens.read(reinterpret_cast<char *>(&dummy), dSIZE);
              dens[i] = dummy;
            }
        }
      else
        for(int i=0; i<NN; i++)
          fdens.read(reinterpret_cast<char *>(&dens[i]), SIZE);
      fdens.close();
      cout << "done!" << endl;
    }

  cout << "  rescaling velocities ... ";
  for(unsigned int k=1; k<dim-1; k++)
    for(unsigned int j=1; j<dim-1; j++)
      for(unsigned int i=1; i<dim-1; i++)
        {
          velx[k*dim*dim + j*dim + i] *= vel_scale;
          vely[k*dim*dim + j*dim + i] *= vel_scale;
          velz[k*dim*dim + j*dim + i] *= vel_scale;
        }
  cout << "done" << endl;

  // find density maximum
  if(densfile != "")
    {
      for(unsigned int k=1; k<dim-1; k++)
	for(unsigned int j=1; j<dim-1; j++)
	  for(unsigned int i=1; i<dim-1; i++)
	    if(dens[k*dim*dim + j*dim + i] > rho_max)
	      rho_max = dens[k*dim*dim + j*dim + i];
    }
  else
    rho_max = plummer(0.0,
		      plummer_sets[plu_set][1],
		      plummer_sets[plu_set][0],
		      plummer_sets[plu_set][2]);
  

  cout << "  calculating divergence ... ";
  // go through all the cells and calculate the divergence
  double divxmin = 0.0;
  double divxmax = 0.0;
  double divymin = 0.0;
  double divymax = 0.0;
  double divzmin = 0.0;
  double divzmax = 0.0;
  for(unsigned int k=1; k<dim-1; k++)
    for(unsigned int j=1; j<dim-1; j++)
      for(unsigned int i=1; i<dim-1; i++)
        {
          divx[k*dim*dim + j*dim + i]
            = (velx[k*dim*dim + j*dim + i+1] - velx[k*dim*dim + j*dim + i-1])/(2.0*dx);
          divy[k*dim*dim + j*dim + i]
            = (vely[k*dim*dim + (j+1)*dim + i] - vely[k*dim*dim + (j-1)*dim + i])/(2.0*dx);
          divz[k*dim*dim + j*dim + i]
            = (velz[(k+1)*dim*dim + j*dim + i] - velz[(k-1)*dim*dim + j*dim + i])/(2.0*dx);
          divxyz[k*dim*dim + j*dim + i]
            = (divx[k*dim*dim + j*dim + i] +
               divy[k*dim*dim + j*dim + i] +
               divz[k*dim*dim + j*dim + i]);
          if(divx[k*dim*dim + j*dim + i] < divxmin) divxmin = divx[k*dim*dim + j*dim + i];
          if(divx[k*dim*dim + j*dim + i] > divxmax) divxmax = divx[k*dim*dim + j*dim + i];
          if(divy[k*dim*dim + j*dim + i] < divymin) divymin = divy[k*dim*dim + j*dim + i];
          if(divy[k*dim*dim + j*dim + i] > divymax) divymax = divy[k*dim*dim + j*dim + i];
          if(divz[k*dim*dim + j*dim + i] < divzmin) divzmin = divz[k*dim*dim + j*dim + i];
          if(divz[k*dim*dim + j*dim + i] > divzmax) divzmax = divz[k*dim*dim + j*dim + i];
        }

  cout << "done!" << endl;
  cout << "    div[x,y,z][min,max]    [1/s] "
       << "[" << divxmin << ";" << divxmax << "] "
       << "[" << divymin << ";" << divymax << "] "
       << "[" << divzmin << ";" << divzmax << "]" << endl;
  cout << "    div[x,y,z][min,max]^-1 [ky]  "
       << "[" << 1.0/divxmin/kyr << ";" << 1.0/divxmax/kyr << "] "
       << "[" << 1.0/divymin/kyr << ";" << 1.0/divymax/kyr << "] "
       << "[" << 1.0/divzmin/kyr << ";" << 1.0/divzmax/kyr << "]" << endl;

  // put data in radial bins
  //#########################################
  std::vector<double> radiusbins    (dim,0.0);    // radius
  std::vector<long>   bin_N_cell    (dim/2,0);    // number of cells per bin

  std::vector<double> mass_bins     (dim/2,0.0);  // mass
  std::vector<double> mass_bins2    (dim/2,0.0);  // normal. mass to rho_max

  std::vector<double> div_bins      (dim/2,0.0);  // divergence
  std::vector<double> div_mass_bins (dim/2,0.0);  // div. weighted with mass
  std::vector<double> div_mass_bins2(dim/2,0.0);  // div. weighted with mass normal. to rho_max
  std::vector<double> div_tff_bins  (dim/2,0.0);  // div. weighted with free-fall time

  std::vector<double> vrad_bins     (dim/2,0.0);  // radial velocity
  std::vector<double> vrad_mass_bins(dim/2,0.0);  // rad vel weighted with mass


  // set radial bin bounds
  double dx   = 1.0/(double)dim;
  double dvol = pow(box_scale*dx,3);
  for(unsigned int i=0; i<dim; i++)
    radiusbins[i] = ((double)i + 0.5) * dx;
  cout << "  radial setup ... done" << endl;

  double total_mass = 0.0;
  long   ibin_max   = (long)(max_radius_norm/dx);
  long   ibin_min   = (long)(min_radius_norm/dx);
  double density    = 0.0;
  for(unsigned int k=2; k<dim-2; k++)
    for(unsigned int j=2; j<dim-2; j++)
      for(unsigned int i=2; i<dim-2; i++)
        {
          double di = ((double)i+0.5)*dx - 0.5;
          double dj = ((double)j+0.5)*dx - 0.5;
          double dk = ((double)k+0.5)*dx - 0.5;
          double radius = sqrt(pow(di, 2.0) +
                               pow(dj, 2.0) +
                               pow(dk, 2.0));

          double div  = divxyz[k*dim*dim + j*dim + i];
          //double div  = (divx[k*dim*dim + j*dim + i] +
          //             divy[k*dim*dim + j*dim + i] +
          //             divz[k*dim*dim + j*dim + i]);
          double vrad = (velx[k*dim*dim + j*dim + i] * di +
                         vely[k*dim*dim + j*dim + i] * dj +
                         velz[k*dim*dim + j*dim + i] * dk)/radius;
          long ibin = (long)(radius/dx);

          if(densfile != "")
            density = dens[k*dim*dim + j*dim + i];
          else
            density = plummer(radius*box_scale,
                              plummer_sets[plu_set][1],
                              plummer_sets[plu_set][0],
                              plummer_sets[plu_set][2]);

          double mass     = dvol * density;
          double normmass = dvol * density / rho_max;

          double t_ff = sqrt(3.0*pi/(32.0*G*density));

          // calculate gravitational to hydrodyn. compression
          compr[k*dim*dim + j*dim + i] = t_ff * div;

          if((ibin >= 0) && (ibin >= ibin_min) && (ibin < dim/2) && (ibin <= ibin_max))
            {
              bin_N_cell[ibin]++;

              total_mass += mass;

              mass_bins[ibin]      += mass;

              div_bins[ibin]       += div;
              div_mass_bins[ibin]  += div * mass;
              //div_mass_bins2[ibin] += div * normmass;
	      if(div < 0.0)
		{
		  div_mass_bins2[ibin]  += div;
		  mass_bins2[ibin]      += mass;
		}
              div_tff_bins[ibin]   += div * t_ff;

              vrad_bins[ibin]      += vrad;
              vrad_mass_bins[ibin] += vrad * mass;
            }
        }
  cout << "  total mass in cloud: " << total_mass << endl;


  cout << "  printing output" << endl;
  // print data to file
  fstream outfile(outfilename.c_str(),ios::out);
  double accum_div         = 0.0;
  double accum_vrad        = 0.0;
  double accum_div_mass    = 0.0;
  double accum_div_mass2   = 0.0;
  double accum_vrad_mass   = 0.0;
  double accum_div_tff     = 0.0;
  double accum_mass        = 0.0;
  double accum_mass2       = 0.0;
  for(unsigned int i=0; i<=ibin_max; i++)
    {
      accum_div         += div_bins[i];
      accum_vrad        += vrad_bins[i];
      accum_div_mass    += div_mass_bins[i];
      accum_div_mass2   += div_mass_bins2[i];
      accum_vrad_mass   += vrad_mass_bins[i];
      accum_div_tff     += div_tff_bins[i];
      accum_mass        += mass_bins[i];
      accum_mass2       += mass_bins2[i];

      outfile << std::setw(13) << (double)i*dx*box_scale
              << std::setw(13) << div_bins[i]
              << std::setw(13) << vrad_bins[i]
              << std::setw(13) << accum_div
              << std::setw(13) << accum_vrad
              << std::setw(13) << div_mass_bins[i] /total_mass
              << std::setw(13) << vrad_mass_bins[i]/total_mass
              << std::setw(13) << accum_div_mass   /total_mass
              << std::setw(13) << accum_vrad_mass  /total_mass
              << std::setw(13) << div_tff_bins[i]
              << std::setw(13) << accum_div_tff
              << std::setw(13) << accum_div_mass   /accum_mass
              << std::setw(13) << accum_div_mass2
              << std::setw(13) << accum_div_mass2  /accum_mass2
	      << std::setw(13) << accum_mass2
              << endl;
    }
  outfile << "# accum_div_mass  = " << accum_div_mass/total_mass << endl;
  outfile << "# accum_vrad_mass = " << accum_vrad_mass/total_mass << endl;
  outfile << "# accum_div_tff   = " << accum_div_tff << endl;
  outfile.close();

  // print compression ratio
  // (a) sheet
  if(icompr > 0)
    {
      cout << " calculating compression ratio (sheet " << icompr << ")" << endl;
      fstream comprfile(comprfname.c_str(),ios::out);
      for(unsigned int j = 1; j<dim-1; j++)
        {
          for(unsigned int i = 1; i<dim-1; i++)
            comprfile << setw(13) << (double)i*dx*box_scale
                      << setw(13) << (double)j*dx*box_scale
                      << setw(13) << compr[icompr*dim*dim + j*dim + i] << endl;
          comprfile << endl;
        }
      comprfile.close();
    }

  // (b) integrated
  if(icompr < 0)
    {
      cout << " calculating compression ratio (integrated along " << icompr << " axis (-1=x, -2=y, -3=z))" << endl;
      fstream comprfile(comprfname.c_str(),ios::out);
      for(unsigned int j = 1; j<dim-1; j++)
        {
          for(unsigned int i = 1; i<dim-1; i++)
            {
              double sum = 0.0;
	      double divsum = 0.0;
	      double divsum2 = 0.0;
              if(icompr == -1)
                {
                  for(unsigned int k = 1; k<dim-1; k++)
		    {                  
		      sum += compr[j*dim*dim + i*dim + k];
		      divsum += divxyz[j*dim*dim + i*dim + k];
		      divsum2 += divxyz[j*dim*dim + i*dim + k]*dens[j*dim*dim + i*dim + k];
		    }
                  comprfile << setw(13) << (double)i*dx*box_scale
                            << setw(13) << (double)j*dx*box_scale
                            << setw(13) << sum
                            << setw(13) << divsum
                            << setw(13) << divsum2 << endl;
                }
              if(icompr == -2)
                {
                  for(unsigned int k = 1; k<dim-1; k++)
                    sum += compr[j*dim*dim + k*dim + i];
                  comprfile << setw(13) << (double)i*dx*box_scale
                            << setw(13) << (double)j*dx*box_scale
                            << setw(13) << sum << endl;
                }
              if(icompr == -3)
                {
                  for(unsigned int k = 1; k<dim-1; k++)
                    sum += compr[k*dim*dim + j*dim + i];
                  comprfile << setw(13) << (double)i*dx*box_scale
                            << setw(13) << (double)j*dx*box_scale
                            << setw(13) << sum << endl;
                }
            }
          comprfile << endl;
        }
      comprfile.close();
    }

  if(NNmax>0)
    {
      // use separate array to store only non-boundary values
      std::vector<float> div_tmp((long)pow(dim-4,3.0));
      long cnt = 0;
      for(unsigned int k=2; k<dim-2; k++)
        for(unsigned int j=2; j<dim-2; j++)
          for(unsigned int i=2; i<dim-2; i++)
            {
              div_tmp[cnt] = divxyz[k*dim*dim + j*dim + i];
              cnt++;
            }

      cout << "  maximum inverse convergence [kyr]" << endl;
      sort(div_tmp.begin(), div_tmp.end());
      for(int i = 0; i< NNmax; i++)
        {
          cout << "     " << i << "  " << -1.0/div_tmp[i]/kyr << endl;
        }
      fstream divfile(((string)argv[1] + "-divxyz.dat").c_str(), ios::out);
      cnt = 0;
      for(unsigned int k=2; k<dim-2; k++)
        for(unsigned int j=2; j<dim-2; j++)
          for(unsigned int i=2; i<dim-2; i++)
            {
              double di = ((double)i+0.5)*dx - 0.5;
              double dj = ((double)j+0.5)*dx - 0.5;
              double dk = ((double)k+0.5)*dx - 0.5;
              double radius = sqrt(pow(di, 2.0) +
                                   pow(dj, 2.0) +
                                   pow(dk, 2.0));
              divfile << cnt << "  "
                      << radius << "  "
                      << -1.0/div_tmp[cnt]/kyr << "  "
                      << -1.0/divxyz[k*dim*dim + j*dim + i]/kyr << "  "
                      << di << " " << dj << " " << dk << endl;
              cnt++;
            }
      divfile.close();
    }

  return 0;
}
